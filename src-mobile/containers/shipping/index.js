import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import { Form, Field } from 'react-final-form';
import { InputText, Select2, Cover, Spinner, Button, Checkbox } from "../../../components/common";
import { COUNTRY_LIST } from "../../../config";
import UserModel from "../../../models/user";

class Shipping extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.renderBilling = this.renderBilling.bind(this);
        this.renderShipping = this.renderShipping.bind(this);
        this.state = {
            full_name: '',
            country: 0,
            address1: '',
            address2: '',
            city: '',
            postal_code: '',
            mode: 0,
            isLoading: false
        }
        this.form = null;
    }
    componentDidMount(){
        this.setState({isLoading: true});
        UserModel.getUserDetail({email: this.props.auth})
        .then((user) => {
            const {full_name, country, address1, address2, city, postal_code} = user;
            this.setState({isLoading: false, full_name, country, address1, address2, city, postal_code});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    componentWillUnmount(){
        this.form = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            const {full_name, country, address1, address2, city, postal_code} = values;
            UserModel.editUser({email: this.props.auth, full_name, country, address1, address2, city, postal_code})
            .then(result => {
                this.props.history.push('/payment');
                resolve();
            })
            .catch(error => {
                resolve();
            })
        })
    }
    renderShipping(){
        const renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="full_name_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="full_name_1">Full Name</label>
                            <InputText {...input} type="text" placeholder="Full Name"
                                disabled={this.state.step} id="full_name_1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="country_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="country_1">Country</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => {
                                    if(is.existy(instance))
                                        instance.refreshList(COUNTRY_LIST);
                                }} {...input} 
                                    placeholder="Select Country" id="country_1"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }
                                />
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address1_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address1_1">Address 1</label>
                            <InputText {...input} type="text" placeholder="Address 1"
                                disabled={this.state.step} id="address1_1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address2_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address2_1">Address 2</label>
                            <InputText {...input} type="text" placeholder="Address 2"
                                disabled={this.state.step} id="address2_1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="city_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="city_1">City</label>
                            <InputText {...input} type="text" placeholder="City"
                                disabled={this.state.step} id="city_1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="postal_code_1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="postal_code_1">Postal Code</label>
                            <InputText {...input} type="text" placeholder="Postal Code"
                                disabled={this.state.step} id="postal_code_1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return renderFields;
    }
    renderBilling(){
        const renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="full_name">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="full_name">Full Name</label>
                            <InputText {...input} type="text" placeholder="Full Name"
                                disabled={this.state.step} id="full_name"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="country">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="country">Country</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => {
                                    if(is.existy(instance))
                                        instance.refreshList(COUNTRY_LIST);
                                }} {...input} 
                                    placeholder="Select Country" id="country"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }
                                />
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address1">Address 1</label>
                            <InputText {...input} type="text" placeholder="Address 1"
                                disabled={this.state.step} id="address1"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address2">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address2">Address 2</label>
                            <InputText {...input} type="text" placeholder="Address 2"
                                disabled={this.state.step} id="address2"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="city">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="city">City</label>
                            <InputText {...input} type="text" placeholder="City"
                                disabled={this.state.step} id="city"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="postal_code">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="postal_code">Postal Code</label>
                            <InputText {...input} type="text" placeholder="Postal Code"
                                disabled={this.state.step} id="postal_code"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    initialValues={{
                        full_name: this.state.full_name,
                        country: this.state.country,
                        address1: this.state.address1,
                        address2: this.state.address2,
                        city: this.state.city,
                        postal_code: this.state.postal_code
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(!values.full_name)
                                errors.full_name = "Full Name must be required";

                            if(!values.country)
                                errors.country = "Country must be required";

                            if(!values.address1)
                                errors.address1 = "Address 1 must be required";

                            if(!values.city)
                                errors.city = "City must be required";

                            if(!values.postal_code)
                                errors.postal_code = "Postal Code must be required";

                            if(this.state.mode){
                                if(!values.full_name_1)
                                errors.full_name_1 = "Full Name must be required";

                                if(!values.country_1)
                                    errors.country_1 = "Country must be required";

                                if(!values.address1_1)
                                    errors.address1_1 = "Address 1 must be required";

                                if(!values.city_1)
                                    errors.city_1 = "City must be required";

                                if(!values.postal_code_1)
                                    errors.postal_code_1 = "Postal Code must be required";
                            }
                                
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return (
                            <React.Fragment>
                                <form onSubmit={handleSubmit}>
                                    <div className="uk-inline uk-width-1-1">
                                        {submitting || this.state.isLoading && <Cover/>}
                                        {submitting || this.state.isLoading && <Spinner/>}
                                        <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                            {renderFields}
                                        </div>
                                    </div>
                                    <div className="uk-width-1-1">
                                        <Checkbox label="As my billing information"
                                            checked={this.state.mode ? false : true}
                                            onChange={() => this.setState({mode: !this.state.mode})}/>
                                    </div>
                                    {
                                        this.state.mode
                                        ?
                                        <div className="uk-margin-top">
                                            <h3 className="uk-margin-remove-top uk-margin-top">Shipping Information</h3>
                                            <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                                {this.renderShipping()}
                                            </div>
                                        </div>
                                        : null
                                    }
                                    <div className="uk-width-1-1 uk-margin">
                                        <Button className="uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                            Continue To Payment
                                        </Button>
                                    </div>
                                </form>
                            </React.Fragment>
                        )
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div className="uk-margin-bottom">
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Shipping & Billing</div>
                </div>
                <div className="uk-container uk-margin-top">
                    <h3 className="uk-margin-remove-top">Billing Information</h3>
                    {this.renderBilling()}
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(Shipping));