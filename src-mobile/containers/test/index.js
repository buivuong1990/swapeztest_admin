import React from "react";
import {SERVER_IMG} from "../../../config";

class Dev extends React.Component{
    constructor(props){
        super(props);
    }
    componentDidMount(){
        console.log('did mount dev');
    }
    componentWillUnmount(){
        console.log('will unmount dev');
    }
    handlerClick(){
        if(is.existy(this.props.onClick))
            this.props.onClick();
    }
    render(){
        return (
            <button className="uk-button"
                onClick={this.handlerClick.bind(this)}>Dev</button>
        )
    }
}

class TestLayout extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isDev: true
        }
    }
    componentDidMount(){
        console.log('did mount test');
    }
    componentWillUnmount(){
        console.log('will unmount test');
    }
    handlerClickDev(){
        this.setState({isDev: false});
    }
    render(){
        return (
           <div>
               <div className="uk-padding">
                    {this.state.isDev && <Dev onClick={this.handlerClickDev.bind(this)}/>}
                </div>
           </div>
        )
    }
}

export default TestLayout;