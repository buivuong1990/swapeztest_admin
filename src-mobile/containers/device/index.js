import React from "react";

import DeviceModel from "../../../models/device";
import { SERVER_DOMAIN } from "../../../config";
import { Cover, Spinner, Radio, Checkbox } from "../../../components/common";
import RelatedDevices from "../../../components/mobile/relatedDevices";
import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";

import UserModel from "../../../models/user";
import withStorage from "../../../hoc/storage";

class DeviceDetail extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            list :[]
        }
        this.renderPrice = this.renderPrice.bind(this);
        this.radioValue = '';
    }
    handlerListImage(){
        this.setState({isLoading:true});
        DeviceModel.getListImage({device_id:this.id})
            .then((list)=>{
                this.setState({isLoading:false,list:list},()=>{
                    UIkit.slideshow($(this.slideshow));
                });
            })
            .catch(() => {
                this.setState({ isLoading: true });
            })
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        this.refreshDetail();
        this.handlerListImage();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceDetail({id: this.id, email: this.props.auth})
        .then((result) => {
            this.setState({isLoading: false, detail: result});
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerAddToCart(){
        const type = this.state.detail.type;
        if(type < 3){
           this.handlerActionType(type);
        }else if(type === 3){
            if(this.radioValue === ''){
                UIkit.notification({message: 'Must Choose Sale Or Exchange !!!', status: 'danger', pos: 'top-left'});
                return;   
            }
            if(this.radioValue === 'sale')
                this.handlerActionType(1);
            else if(this.radioValue === 'exchange')
                this.handlerActionType(2);
        }
    }
    handlerActionType(type){
        this.setState({isLoading: true});
        this.handlerActionCart(type);
    }
    handlerActionCart(type){
        const {available_id, id} = this.state.detail;
        UserModel.postItemToCart({available_id, device_id: id, email: this.props.auth, type})
        .then(() => {
            this.props.addCart();
            this.refreshDetail();
        })
    }
    handlerChangeRadio(event){
        const value = event.target.value;
        this.radioValue = value;
    }
    handlerWishlist(){
        const {isWishlist, id} = this.state.detail;
        this.setState({isLoading: true});
        if(!isWishlist){
            UserModel.addWishlist({email: this.props.auth, device_id: id})
            .then(() => {
                this.refreshDetail();
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }else if(isWishlist){
            UserModel.removeWishlist({id: isWishlist})
            .then(() => {
                this.refreshDetail();
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }
    }
    renderPrice(){
        const type = this.state.detail.type;
        if(type === 1){
            return (
                <li>
                    <div className="uk-flex-middle uk-flex uk-flex-between">
                        <div>
                            <Radio label={'Sale'} mode="basic" fontSize={'16px'} value="sale" name="detail_available"
                                disabled={this.state.detail.cart_id ? true: false}
                                defaultChecked={true}/>
                        </div>
                        <span className="uk-text-large">{accounting.formatMoney(this.state.detail.sale_price)}</span>
                    </div>
                </li>
            )
        }else if(type === 2){
            return (
                <li>
                    <div className="uk-flex-middle uk-flex uk-flex-between">
                        <div>
                            <Radio label={'Exchange'} mode="basic" fontSize={'16px'} value="exchange" name="detail_available"
                                disabled={this.state.detail.cart_id ? true: false}
                                defaultChecked={true}/>
                            <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                <i className="fa fa-exchange"/>
                                <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {this.state.detail.exchange_name}</div>
                            </div>
                        </div>
                        <span className="uk-text-large">{accounting.formatMoney(this.state.detail.exchange_price)}</span>
                    </div>
                </li>
            )
        }else if(type === 3){
            return (
                <React.Fragment>
                    <li>
                        <div className="uk-flex-middle uk-flex uk-flex-between">
                            <div>
                                <Radio label={'Sale'} mode="basic" fontSize={'16px'} value="sale" name="detail_available"
                                    defaultChecked={Number(this.state.detail.cart_type) === 1 ? true : false}
                                    disabled={this.state.detail.cart_id ? true: false}
                                    onChange={this.handlerChangeRadio.bind(this)}/>
                            </div>
                            <span className="uk-text-large">{accounting.formatMoney(this.state.detail.sale_price)}</span>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex-middle uk-flex uk-flex-between">
                            <div>
                                <Radio label={'Exchange'} mode="basic" fontSize={'16px'} value="exchange" name="detail_available"
                                    defaultChecked={Number(this.state.detail.cart_type) === 2 ? true: false}
                                    disabled={this.state.detail.cart_id ? true: false}
                                    onChange={this.handlerChangeRadio.bind(this)}/>
                                <div className="uk-flex uk-text-meta uk-flex-middle uk-margin-xsmall-top">
                                    <i className="fa fa-exchange"/>
                                    <div className="uk-margin-xsmall-left uk-text-meta uk-text-truncate">Exchange With: {this.state.detail.exchange_name}</div>
                                </div>
                            </div>
                            <span className="uk-text-large">{accounting.formatMoney(this.state.detail.exchange_price)}</span>
                        </div>
                    </li>
                </React.Fragment>
            )
        }
    }
    render(){
       
        return (
            <div>
                <div className="uk-inline uk-width-1-1 uk-container">
                    {this.state.isLoading && <Cover/>}
                    {this.state.isLoading && <Spinner/>}
                    <div className="uk-padding-small">
                    {
                        this.state.list.length > 0
                        ?
                        <div className="uk-position-relative uk-visible-toggle" ref={instance => this.slideshow = instance}>
                            <ul className="uk-slideshow-items uk-height-large" data-uk-lightbox="animation: slide">
                            {
                                this.state.list.map((l, key) => {
                                    return(
                                        <li key={key}>
                                            <a className="uk-inline" href={SERVER_DOMAIN+'devices/'+l.url} alt={this.state.detail.device_name}>
                                                <img src={SERVER_DOMAIN+'devices/'+l.url} alt={this.state.detail.device_name}/>
                                            </a>
                                        </li>
                                    )
                                })
                            }
                            </ul>
                        </div>
                        :
                        <img data-src={SERVER_DOMAIN+'no-image.png'} uk-img="true" />
                    }
                       
                    </div>

                    <div className="uk-flex uk-flex-between uk-flex-middle uk-margin">
                        <h4 className="uk-text-primary uk-text-bold uk-margin-remove">{this.state.detail.device_name}</h4>
                        <div>
                            {
                                !this.state.detail.cart_id
                                ?
                                <span className="uk-margin-left">
                                    {
                                        this.state.detail.isWishlist
                                        ?
                                        <Checkbox mode="heart" fontSize={18} defaultChecked={true}
                                            onClick={this.handlerWishlist.bind(this)}/>
                                        :
                                        <Checkbox mode="heart" fontSize={18}
                                            onClick={this.handlerWishlist.bind(this)}/>
                                    }
                                </span>
                                : null
                            }
                            <a className="uk-link-reset uk-margin-small-left"
                                onClick={() => this.props.history.push('/compare/'+this.id)}>
                                <i className="fa fa-compress" style={{fontSize: 18}}/>
                            </a>
                        </div>
                    </div>
                    <hr/>
                    <div>
                        <ul className="uk-list uk-list-large">
                            {this.renderPrice()}
                        </ul>
                    </div>
                    <div>
                        {
                            this.state.detail.cart_id 
                            ?
                            <button className="uk-button uk-width-1-1 uk-button-secondary" disabled={true}>Product Is In Your Cart</button>
                            :
                            <button className="uk-button uk-button-primary uk-width-1-1"
                                onClick={this.handlerAddToCart.bind(this)} ref={instance => this.atcRef = instance}>Add To Cart</button>
                        }
                    </div>
                </div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-top uk-margin-bottom">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Device Information</div>
                </div>
                <div className="uk-margin-top uk-container uk-margin-bottom">
                    <ul className="uk-list uk-list-large">
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">Category:</div>
                                <span>{this.state.detail.category_name}</span>
                            </div>
                        </li>
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">Brand:</div>
                                <span>{this.state.detail.brand_name}</span>
                            </div>
                        </li>
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">Color:</div>
                                <span>{this.state.detail.color_name}</span>
                            </div>
                        </li>
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">RAM:</div>
                                <span>{this.state.detail.ram_name}</span>
                            </div>
                        </li>
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">Capacity:</div>
                                <span>{this.state.detail.capacity_name}</span>
                            </div>
                        </li>
                        <li>
                            <div className="uk-flex uk-flex-between">
                                <div className="uk-text-bold">Condition:</div>
                                <span>{this.state.detail.device_condition} %</span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-top uk-margin-bottom">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Related Devices</div>
                </div>
                <div>
                    <RelatedDevices device_id={this.id}/>
                </div>
            </div>
        )

    
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectDeviceDetail = connect(mapStateToProps, mapDispatchToProps)(withStorage(DeviceDetail));

export default ConnectDeviceDetail;