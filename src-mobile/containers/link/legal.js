import React from "react";

class Legal extends React.Component{
    render(){
        return(
            <div>
                <div className="uk-container uk-padding">
                    <h3>Swap-ez Legal</h3>
                    <p>Before you purchase a new or refurbished hardware product from Swap-ez, you may review the terms and conditions of Swap-ez’s limited warranty including limitations and exclusions.</p>
                    <h3>Software License Agreements</h3>
                    <p>Before acquiring Swap-ez software or hardware products, you may review the terms and conditions of Swap-ez's end user software license agreements.</p>
                    <h3>Sales and Support</h3>
                    <p>Details about the policies, terms, and conditions for the purchase, support, and&nbsp;servicing of your Swap-ez products.</p>
                    <h3>Internet Services</h3>
                    <p>Policies and terms&nbsp;that&nbsp;govern Swap-ez online&nbsp;services, including Swap-ez Media Services, iCloud, and our Privacy Policy.</p>
                </div>
            </div>
        )
    }
}
export default Legal;