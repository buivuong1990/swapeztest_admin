import React from "react";

class Term extends React.Component{
    render(){
        return(
            <div>
                <div className="uk-container uk-padding">
                    <h3>Terms of Use</h3>
                    <p>Swap-ez reserves the right, at its sole discretion, to change, modify, add or remove portions of these Terms of Use, at any time. It is your responsibility to check these Terms of Use periodically for changes. Your continued use of the Site following the posting of changes will mean that you accept and agree to the changes. As long as you comply with these Terms of Use, Swap-ez grants you a personal, non-exclusive, non-transferable, limited privilege to enter and use the Site.</p>

                    <h3>Content</h3>
                    <p>All text, graphics, user interfaces, visual interfaces, photographs, trademarks, logos, sounds, music, artwork and computer code (collectively, "Content"), including but not limited to the design, structure, selection, coordination, expression, "look and feel" and arrangement of such Content, contained on the Site is owned, controlled or licensed by or to Swap-ez, and is protected by trade dress, copyright, patent and trademark laws, and various other intellectual property rights and unfair competition laws.</p>
                    
                    <h3>Your Use of the Site</h3>
                    <p>You may not use any "deep-link", "page-scrape", "robot", "spider" or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Site or any Content, or in any way reproduce or circumvent the navigational structure or presentation of the Site or any Content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Site. Swap-ez reserves the right to bar any such activity.</p>
                    <p>You may not attempt to gain unauthorized access to any portion or feature of the Site, or any other systems or networks connected to the Site or to any Swap-ez server, or to any of the services offered on or through the Site, by hacking, password "mining" or any other illegitimate means.</p>

                </div>
            </div>
        )
    }
}
export default Term;