import React from "react";

class SiteMap extends React.Component{
    render(){
        return(
            <div>
                <div className="uk-container uk-padding">
                    <h3>Site Map</h3>
                    <p className="uk-text-bold uk-margin-xsmall-bottom">Account</p>
                    <ul className="uk-list uk-margin-remove">
                        <li><a>Login</a></li>
                        <li><a>Register</a></li>
                        <li><a>My account</a></li>
                        <li><a>My wishlist</a></li>
                    </ul>
                    <p className="uk-text-bold uk-margin-xsmall-bottom">Category</p>
                    <ul className="uk-list uk-margin-remove">
                        <li><a>Smart phones</a></li>
                        <li><a>Tablets</a></li>
                        <li><a>Smart Watches</a></li>
                    </ul>
                    <p className="uk-text-bold uk-margin-xsmall-bottom">Purpose</p>
                    <ul className="uk-list uk-margin-remove">
                        <li><a>Sale</a></li>
                        <li><a>Buy</a></li>
                        <li><a>Exchange</a></li>
                    </ul>
                    <p className="uk-text-bold uk-margin-xsmall-bottom">About Swap-ez</p>
                    <ul className="uk-list uk-margin-remove">
                        <li><a>Newroom</a></li>
                        <li><a>Leadership</a></li>
                        <li><a>Job Opportunities</a></li>
                        <li><a>Investors</a></li>
                        <li><a>Contact Swap-ez</a></li>
                    </ul>
                    <p className="uk-text-bold uk-margin-xsmall-bottom">Product</p>
                    <ul className="uk-list uk-margin-remove">
                        <li><a>Smart Phones</a></li>
                        <li><a>Tablets</a></li>
                        <li><a>Smart Watches</a></li>
                        <li><a>Sell Products</a></li>
                        <li><a>Exchange Products</a></li>
                    </ul>
                </div>
            </div>
        )
    }
}
export default SiteMap;