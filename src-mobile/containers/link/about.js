import React from "react";
import {SERVER_IMG} from "../../../config";
class AboutUs extends React.Component{
    render(){
        return(
            <div>
                <div>
                    <img data-src={SERVER_IMG+'about-us.jpg'} uk-img="true"/>
                </div>
                <div className="uk-container uk-padding">
                    <h3 className="uk-text-center">We bring amazing people together to make amazing things happen.</h3>
                    <p className="uk-text-xlarge uk-text-center">We’re a diverse collective of thinkers and doers, continuously reimagining our products and practices to help people do what they love in new ways. That innovation is inspired by a shared commitment to great work — and to each other. Because learning from the people here means we’re learning from the best.</p>
                    <p>Every new product, service, or feature we invent is the result of people working together to make each other’s ideas stronger. That happens here because every one of us strives toward a common goal — creating the best customer experiences. Just one example: The incredibly advanced camera features in every new iPhone are not the result of a few dozen people. They’re the product of hundreds of optical technology experts who complement each other’s thinking. Multiply that effort across every feature in every Swap-ez product, and you’ll get an idea of how important collaboration is here.</p>
                </div>
            </div>
        )
    }
}
export default AboutUs;