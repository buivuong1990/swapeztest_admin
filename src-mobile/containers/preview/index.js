import React from "react";
import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import UserModel from "../../../models/user";

import { Cover, Spinner, Modal } from "../../../components/common";
import { SERVER_DOMAIN } from "../../../config";

import withStorage from "../../../hoc/storage";
import Functions from "../../../functions";

class Preview extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            selectedCart: {},
            list: [],
            total: 0
        }
        this.renderMoney = this.renderMoney.bind(this);
        this.renderExchangeWith = this.renderExchangeWith.bind(this);
        this.listItemInstance = [];
    }
    componentDidMount(){
        if(is.existy(localStorage.getItem('listPreview')) && is.existy(localStorage.getItem('listPreviewTotal'))){
            const list = JSON.parse(localStorage.getItem('listPreview'));
            const total = Number(localStorage.getItem('listPreviewTotal'));
            this.setState({list: list, total: total});
        }else
            this.props.history.push('/cart');
    }
    handlerGoToDevice(l){
        this.props.history.push('/device/'+l.device_id);
    }
    renderMoney(l){
        return (
            <b>{accounting.formatMoney(l.real_price)}</b>
        )
    }
    renderExchangeWith(l){
        if(!l.proposal_id)
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_name}</b>
                </div>
            )
        else
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_proposal_real_device}</b>
                </div>
            )
    }
    renderCartList(){
        return (
            <div>
                {
                    this.state.list.map((l, key) => {
                        return (
                            <div className={"swiper-container "+(l.prevent ? 'uk-background-danger uk-light': '')} key={key}>
                                <div className="swiper-wrapper">
                                    <div className="swiper-slide content">
                                        <div className="uk-flex uk-flex-middle uk-padding-small">
                                            <div>
                                            {
                                                l.thumb
                                                ?
                                                <a onClick={() => this.handlerGoToDevice(l)}>
                                                    <img className="uk-img uk-cursor" data-src={SERVER_DOMAIN+'devices/'+l.thumb} width="70" height="70" uk-img="true"/>
                                                </a>
                                                :
                                                <a onClick={() => this.handlerGoToDevice(l)}>
                                                    <img src={SERVER_DOMAIN+'no-image.png'} width="70" height="70"/>
                                                </a>
                                            }
                                            </div>
                                            <div className="uk-flex uk-flex-between uk-flex-middle uk-width-1-1">
                                                <div className="uk-margin-left">
                                                    <p className="uk-margin-remove uk-text-small uk-text-bold">{l.device_name}</p>
                                                    <p className="uk-margin-remove uk-text-muted uk-text-small">{l.capacity_name} - {l.color_name}</p>
                                                    <div className="uk-text-small">
                                                        {
                                                            Number(l.cart_type) === 1
                                                            ? <div>Sale</div>
                                                            :
                                                            this.renderExchangeWith(l)
                                                        }
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="uk-text-small">
                                                        {this.renderMoney(l)}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
                <div className="uk-padding-small">
                    <div className="uk-clearfix uk-margin-top">
                        <div className="uk-float-right">
                            <span><b>Total: </b>{accounting.formatMoney(this.state.total)}</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    render(){
        return (
            <div>
                <div>
                    <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                        <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Preview</div>
                    </div>
                    <div className="uk-inline uk-width-1-1 uk-margin">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        {
                            this.state.list.length > 0
                            ? this.renderCartList()
                            : <div className="uk-placeholder uk-text-center">There is no item in cart.</div>
                        }
                    </div>
                    
                    <div className="uk-padding-small">
                        <div className="uk-flex uk-flex-center uk-flex-between">
                            <a className="uk-padding-remove uk-width-1-1 uk-button uk-button-default uk-text-bold uk-text-emphasis"
                                onClick={() => this.props.history.push('/cart')}>Back To Cart</a>
                            {
                                this.state.list.length > 0 && !this.props.anonymous
                                ?
                                <a onClick={() => this.props.history.push('/shipping')}
                                    className="uk-margin-xsmall-left uk-padding-remove uk-width-1-1 uk-button uk-button-secondary uk-text-bold">Confirm</a>
                                : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectPreview = connect(mapStateToProps, mapDispatchToProps)(withStorage(Preview));

export default ConnectPreview;