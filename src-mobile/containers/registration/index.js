import React from "react";
import { SERVER_IMG, QUESTION_LIST, RECAPTCHA_SITE_KEY } from "../../../config";

import { Form, Field } from 'react-final-form';
import { InputText, InputPassword, Cover, Spinner, Button, Select2, Checkbox } from "../../../components/common";

import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";

import withStorage from "../../../hoc/storage";

import UserModel from "../../../models/user";
import {withRouter} from "react-router-dom";

import Recaptcha from "react-recaptcha";

class Register extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isEmailExists: false,
            isCaptchaError: false,
            isRedirect: false,
            isLoadingExists: false
        }
        this.resetForm = null;
    }
    componentWillUnmount(){
        this.resetForm = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            if(this.state.isEmailExists){
                UIkit.notification({message: 'Email Exists', status: 'danger', pos: 'top-left'});
                resolve();
            }else if(this.state.isCaptchaError){
                UIkit.notification({message: 'Must Check Captcha', status: 'danger', pos: 'top-left'});
                resolve();
            }else{
                UserModel.registerUser({email: values.email, password: values.password})
                .then(() => {
                    UIkit.notification({message: 'Register Successfully. Please Login', status: 'primary', pos: 'top-left'});
                    this.props.history.push('/login');
                })
                .catch(() => {
                    resolve();
                })
            }
        })
    }
    handlerCheckExists(event){
        const value = event.target.value;
        if(is.email(value))
            this.setState({isLoadingExists: true});
            UserModel.checkExists({email: value})
            .then((result) => {
                if(Number(result) === 0){
                    this.setState({isLoadingExists: false, isEmailExists: true});
                }else this.setState({isLoadingExists: false, isEmailExists: false});
            })
            .catch(() => {
                this.setState({isLoadingExists: false, isEmailExists: false});
            })
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Register Swap-EZ</div>
                </div>
                <div className="uk-container uk-margin-large-top uk-margin-large-bottom">
                    <div>
                        <Form
                            onSubmit={this.handlerSubmit}
                            validate={values => {
                                    const errors = {};
                                    if(!values.email)
                                        errors.email = "Email must be required";
                                    else if(is.not.email(values.email))
                                        errors.email = "Invalid Email";
                                    else if(values.email.length < 6)
                                        errors.email = "Email must > 6 characters";
                                    else if(values.email.length > 50)
                                        errors.email = "Email must < 50 characters";
                                    
                                    if(!values.password)
                                        errors.password = "Password must be required";
                                    else if(values.password.length < 6)
                                        errors.password = "Password must > 6 characters";
                                    else if(values.password.length > 50)
                                        errors.password = "Password must < 50 characters";

                                    if(!values.confirm_password)
                                        errors.confirm_password = "Confirm Password must be required";
                                    else if(values.confirm_password !== values.password)
                                        errors.confirm_password = "Confirm Password does not match";

                                    if(!values.question)
                                        errors.question = "Question must be required";

                                    if(!values.answer)
                                        errors.answer = "Answer must be required";
                                    
                                    return errors;
                                }
                            }
                            render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                                this.resetForm = form.reset;
                                return <form onSubmit={handleSubmit} >
                                    <div className="uk-inline uk-width-1-1">
                                        {submitting && <Cover/>}
                                        {submitting && <Spinner/>}
                                        <Field name="email">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column uk-margin">
                                                <div className="uk-inline uk-width-1-1">
                                                    {this.state.isLoadingExists && <Cover/>}
                                                    {this.state.isLoadingExists && <Spinner/>}
                                                    <InputText {...input} type="text" placeholder="Your Email"
                                                        onBlur={(event) => this.handlerCheckExists(event)}/>
                                                    {
                                                        meta.touched && !this.state.isEmailExists &&
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error || meta.submitError}
                                                        </span>
                                                    }
                                                    {
                                                        this.state.isEmailExists && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            Email Exists
                                                        </span>
                                                    }
                                                </div>
                                            </div>
                                            )}
                                        </Field>
                                        <Field name="password">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column uk-margin">
                                                <InputPassword {...input} placeholder="Your Password"/>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                        <Field name="confirm_password">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column uk-margin">
                                                <InputPassword {...input} placeholder="Confirm Password"/>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                        <Field name="question">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column uk-margin">
                                                <Select2 ref={instance => {
                                                    if(!this.questionRef){
                                                        this.questionRef = instance;
                                                        this.questionRef.refreshList(QUESTION_LIST);
                                                    }
                                                }} {...input} 
                                                    placeholder="Choose Question" id="question"
                                                    onChange={data => {
                                                        input.onChange(Number(data.id))}
                                                    }/>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                        <Field name="answer">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column uk-margin">
                                                <InputPassword {...input} placeholder="Answer"/>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                        <div className="uk-flex uk-margin">
                                            <Recaptcha sitekey={RECAPTCHA_SITE_KEY}
                                                verifyCallback={() => {
                                                    this.setState({isCaptchaError: false});
                                                }}/>
                                        </div>
                                        <div className="uk-flex uk-margin">
                                            <Checkbox label="Receive Our Swap-ez Notifications"/>
                                        </div>
                                        <div className="uk-flex uk-margin">
                                            <Checkbox label="Receive Our Swap-ez Newsletters"/>
                                        </div>
                                        <div className="uk-flex uk-margin-large">
                                            <Button className="uk-width-1-1" type="submit" disabled={submitting || validating} color="primary">
                                                Register
                                            </Button>
                                        </div>
                                    </div>
                                </form>
                            }}
                        >
                        </Form>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectRegister = connect(mapStateToProps, mapDispatchToProps)(withRouter(withStorage(Register)));

export default ConnectRegister;