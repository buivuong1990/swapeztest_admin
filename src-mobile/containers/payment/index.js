import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import { Form, Field } from 'react-final-form';
import { InputText, Select2, Cover, Spinner, Button, Checkbox } from "../../../components/common";
import UserModel from "../../../models/user";

class Payment extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isLoading: false
        }
        this.form = null;
    }
    componentDidMount(){
        $('form').card({
            container: '.card-wrapper',
            messages: {
                validDate: 'expire\ndate',
                monthYear: 'mm/yy'
            }
        });
    }
    componentWillUnmount(){
        this.form = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            if(is.existy(localStorage.getItem('listPreview')) && is.existy(localStorage.getItem('listPreviewTotal'))){
                const listPreview = JSON.parse(localStorage.getItem('listPreview'));
                const totalPreview = Number(localStorage.getItem('listPreviewTotal'));
                UserModel.addOrder({email: this.props.auth, list: listPreview, total: totalPreview})
                .then((result) => {
                    localStorage.removeItem('listPreview');
                    localStorage.removeItem('listPreviewTotal');
                    this.props.app.setState({isMenu: false}, () => {
                        this.props.app.setState({isMenu: true}, () => {
                            UIkit.notification({message: 'Add Order & Transactions successfull, please check your transactions !', status: 'primary', pos: 'top-left'});
                            this.props.history.push('/account/transactions');
                        });
                    });
                    resolve();
                })
                .catch(() => {
                    resolve();
                })
            }else
                this.props.history.push('/bag');
        })
    }
    renderForm(){
        const renderFields = (
            <React.Fragment>
                <div className="uk-width-1-1 uk-margin">
                    <Field name="number">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="number">Card Number</label>
                            <InputText {...input} type="text" placeholder="Your Card Number"
                                disabled={this.state.step} id="number"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-1 uk-margin uk-margin-remove-top">
                    <Field name="name">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="name">Card Name</label>
                            <InputText {...input} type="text" placeholder="Your Card Name"
                                disabled={this.state.step} id="name"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-1 uk-margin uk-margin-remove-top">
                    <div className="uk-grid" uk-grid="true">
                        <div className="uk-width-1-2">
                            <Field name="expiry">
                                {({ input, meta }) => (
                                <div className="uk-flex uk-flex-column">
                                    <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="expiry">Expiry</label>
                                    <InputText {...input} type="text" placeholder="Expiry"
                                        disabled={this.state.step} id="expiry"/>
                                    {
                                        meta.touched && meta.error &&
                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                            {meta.error || meta.submitError}
                                        </span>
                                    }
                                </div>
                                )}
                            </Field>
                        </div>
                        <div className="uk-width-1-2">
                            <Field name="cvc">
                                {({ input, meta }) => (
                                <div className="uk-flex uk-flex-column">
                                    <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="cvc">CVC</label>
                                    <InputText {...input} type="text" placeholder="CVC"
                                        disabled={this.state.step} id="cvc"/>
                                    {
                                        meta.touched && meta.error &&
                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                            {meta.error || meta.submitError}
                                        </span>
                                    }
                                </div>
                                )}
                            </Field>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                        const errors = {};
                        if(!values.number)
                            errors.number = "Card Number must be required";

                        if(!values.name)
                            errors.name = "Card Name must be required";

                        if(!values.expiry)
                            errors.expiry = "Expiry must be required";

                        if(!values.cvc)
                            errors.cvc = "CVC must be required";
                            
                        return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return (
                            <React.Fragment>
                                <form onSubmit={handleSubmit}>
                                    <div className="uk-inline uk-width-1-1">
                                        {submitting || this.state.isLoading && <Cover/>}
                                        {submitting || this.state.isLoading && <Spinner/>}
                                        <div className="uk-grid" uk-grid="true">
                                            <div className="uk-width-1-2@m uk-width-1-1@s card-wrapper"/>
                                            <div className="uk-width-1-2@m uk-width-1-1@s">
                                                {renderFields}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="uk-width-1-1 uk-margin-top uk-flex uk-flex-between">
                                        <Button className="uk-width-auto" type="button" disabled={submitting || validating} color="default"
                                            onClick={() => this.props.history.push('/shipping')}>
                                            Back To Shipping
                                        </Button>
                                        &nbsp;
                                        <Button className="uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                            Checkout
                                        </Button>
                                    </div>
                                </form>
                            </React.Fragment>
                        )
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div className="uk-margin-large-bottom">
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Your Payment</div>
                </div>
                <div className="uk-container uk-margin-top">
                    {this.renderForm()}
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(Payment));