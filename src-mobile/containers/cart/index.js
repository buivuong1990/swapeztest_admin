import React from "react";
import {addCart} from "../../../src/containers/home/actions";
import {connect} from "react-redux";

import UserModel from "../../../models/user";

import { Cover, Spinner, Modal } from "../../../components/common";
import { SERVER_DOMAIN } from "../../../config";

import SaleProposal from "../../../components/mobile/proposal/sale";
import SaleEditProposal from "../../../components/mobile/proposal/saleEdit";

import ExchangeProposal from "../../../components/mobile/proposal/exchange";
import ExchangeEditProposal from "../../../components/mobile/proposal/exchangeEdit";

import withStorage from "../../../hoc/storage";
import Functions from "../../../functions";

class Cart extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            isModalSaleProposal: false,
            isModalExchangeProposal: false,
            isModalEditSaleProposal: false,
            isModalEditExchangeProposal: false,
            selectedCart: {},
            list: []
        }
        this.renderCartList = this.renderCartList.bind(this);
        this.renderMoney = this.renderMoney.bind(this);
        this.renderBtnProposal = this.renderBtnProposal.bind(this);
        this.renderExchangeWith = this.renderExchangeWith.bind(this);
        this.handlerDeleteCartItem = this.handlerDeleteCartItem.bind(this);
        this.listItemInstance = [];
        this.observer = null;
    }
    componentDidMount(){
        Functions.socketReceivedProposal(this.props.socket)
        .subscribe((observer) => {
            this.observer = observer;
            this.refreshListCart();
        })
        this.refreshListCart();
    }
    componentWillUnmount(){
        if(this.observer)
            this.observer.complete();
        for(let i = 0; i < this.listItemInstance.length; i++){
            this.listItemInstance[i].destroy(true, true);
        }
    }
    handlerGoToDevice(l){
        this.props.history.push('/device/'+l.device_id);
    }
    handlerDeleteCartItem(l, key){
        this.setState({isLoading: true});
        UserModel.deleteItemCart({id: l.id})
        .then(() => {
            this.listItemInstance.splice(key, 1);
            this.props.addCart();
            this.refreshListCart();
        })
        .catch(() => {
            this.setState({isLoading: false});
        });
    }
    refreshListCart(){
        this.setState({isLoading: true});
        setTimeout(() => {
            if(this.props.auth){
                UserModel.getListCartByUser({email: this.props.auth})
                .then((result) => {
                    this.setState({list: result, isLoading: false});
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            }
        }, 500)
    }
    renderMoney(l){
        if(Number(l.cart_type) === 1){
            if(l.proposal_id)
                return <div className="uk-flex uk-flex-column">
                        <div className="uk-margin-xsmall-bottom"><b>Proposal Price</b> {accounting.formatMoney(l.sale_proposal_price)}</div>
                        <div><b>Sale Price</b> {accounting.formatMoney(l.sale_price)}</div>
                    </div>
            else
                return <b>{accounting.formatMoney(l.sale_price)}</b>
        }else if(Number(l.cart_type) === 2)
            if(l.proposal_id)
                return <div className="uk-flex uk-flex-column">
                            <div className="uk-margin-xsmall-bottom"><b>Proposal Price</b> {accounting.formatMoney(l.exchange_proposal_price)}</div>
                        </div>
            else
                return <b>{accounting.formatMoney(l.exchange_price)}</b>
    }
    renderExchangeWith(l){
        if(!l.proposal_id)
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_name}</b>
                </div>
            )
        else
            return (
                <div className="uk-text-break">
                    Exchange with <b>{l.exchange_proposal_real_device}</b>
                </div>
            )
    }
    renderBtnProposal(l){
        if(!this.props.anonymous){
            if(Number(l.cart_type) === 1){
                if(l.proposal_id){
                    if(l.proposal_status === 'accepted' || l.proposal_status === 'cancelled')
                        return null;
                    else return <div>
                                <a className="uk-button uk-button-secondary uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => this.setState({isModalEditSaleProposal: true, selectedCart: l})}>
                                    <span uk-icon="icon: pencil"/>
                                    <p className="uk-margin-remove">Edit</p>
                                </a>
                                <a className="uk-button uk-button-danger uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => {
                                    this.setState({isLoading: true})
                                    UserModel.cancelProposal({id: l.proposal_id})
                                    .then(() => {
                                        Functions.socketSendProposal(this.props.socket, l.received_email);
                                        this.refreshListCart();
                                    })
                                    .catch(() => {
                                        this.setState({isLoading: false});
                                    })
                                }}>
                                    <span uk-icon="icon: trash"/>
                                    <p className="uk-margin-remove">Remove</p>
                                </a>
                            </div>
                }else
                    return <a className="uk-button uk-button-secondary uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => this.setState({isModalSaleProposal: true, selectedCart: l})}>
                                <span uk-icon="icon: pencil"/>
                                <p className="uk-margin-remove">Create</p>
                            </a>
            }else if(Number(l.cart_type) === 2)
                if(l.proposal_id){
                    if(l.proposal_status === 'Accepted' || l.proposal_status === 'Cancelled')
                        return null;
                else return <div>
                            <a className="uk-button uk-button-secondary uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => this.setState({isModalEditExchangeProposal: true, selectedCart: l})}>
                                <span uk-icon="icon: pencil"/>
                                <p >Edit</p>
                            </a>
                            <a className="uk-button uk-button-danger uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => {
                                this.setState({isLoading: true})
                                UserModel.cancelProposal({id: l.proposal_id})
                                .then(() => {
                                    Functions.socketSendProposal(this.props.socket, l.received_email);
                                    this.refreshListCart();
                                })
                                .catch(() => {
                                    this.setState({isLoading: false});
                                })
                            }}>
                                <span uk-icon="icon: trash"/>
                                <p>Remove</p>
                            </a>
                        </div>
            }else
                return <a className="uk-button uk-button-secondary uk-padding-small uk-padding-remove-top uk-padding-remove-bottom" onClick={() => this.setState({isModalExchangeProposal: true, selectedCart: l})}>
                            <span uk-icon="icon: pencil"/>
                            <p className="uk-margin-remove">Create</p>
                        </a>
        }else
            return null;
    }
    renderCartList(){
        return (
            <div>
                {
                    this.state.list.map((l, key) => {
                        return (
                            <div className={"swiper-container "+(l.prevent ? 'uk-background-danger uk-light': '')} key={key} ref={(instance) => {
                                new Swiper(instance, {
                                    slidesPerView: 'auto',
                                    initialSlide: 1,
                                    resistanceRatio: 0,
                                    slideToClickedSlide: true,
                                    on: {
                                        init: function(){
                                        },
                                        slideChange: function () {},
                                    }
                                });
                            }}>
                                <div className="swiper-wrapper">
                                    <div className="swiper-slide menu uk-width-auto">
                                        <div className="uk-flex">
                                            {
                                                this.props.auth && !this.props.anonymous
                                                ?
                                                this.renderBtnProposal(l)
                                                :
                                                null
                                            }
                                        </div>
                                    </div>
                                    <div className="swiper-slide content">
                                        <div className="uk-flex uk-flex-middle uk-padding-small uk-padding-remove-top">
                                            <div>
                                            {
                                                l.thumb
                                                ?
                                                <a onClick={() => this.handlerGoToDevice(l)}>
                                                    <img className="uk-img uk-cursor" data-src={SERVER_DOMAIN+'devices/'+l.thumb} width="70" height="70" uk-img="true"/>
                                                </a>
                                                :
                                                <a onClick={() => this.handlerGoToDevice(l)}>
                                                    <img src={SERVER_DOMAIN+'no-image.png'} width="70" height="70"/>
                                                </a>
                                            }
                                            {
                                            Number(l.cart_type) === 2
                                            ?
                                                l.proposal_id 
                                                ?
                                                l.exchange_proposal_real_device_thumb 
                                                ?
                                                <div>
                                                    <a onClick={() => this.handlerGoToDevice(l)}>
                                                        <img src={SERVER_DOMAIN+'devices/'+l.exchange_proposal_real_device_thumb} width="70"/>
                                                    </a>
                                                </div>
                                                :
                                                <div>
                                                    <a onClick={() => this.handlerGoToDevice(l)}>
                                                        <img src={SERVER_DOMAIN+'no-image.png'} width="70"/>
                                                    </a>
                                                </div>
                                                :
                                                null
                                            :
                                            null
                                        }
                                            </div>
                                            
                                            <div className="uk-flex uk-flex-between uk-flex-middle uk-width-1-1">
                                                <div className="uk-margin-left">
                                                    <p className="uk-margin-remove uk-text-small uk-text-bold">{l.device_name}</p>
                                                    <p className="uk-margin-remove uk-text-muted uk-text-small">{l.capacity_name} - {l.color_name}</p>
                                                    {
                                                        l.proposal_id
                                                        ? <p className="uk-margin-remove uk-text-small uk-text-bold">Status: <span className="uk-text-capitalize">{l.proposal_status}</span></p>
                                                        : <p className="uk-margin-remove uk-text-small uk-text-bold">No Proposal</p>
                                                    }
                                                    <div className="uk-text-small">
                                                        {
                                                            Number(l.cart_type) === 1
                                                            ? <div>Sale</div>
                                                            :
                                                            this.renderExchangeWith(l)
                                                        }
                                                    </div>
                                                </div>
                                                <div>
                                                    <div className="uk-text-small uk-text-right">
                                                        {this.renderMoney(l)}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="swiper-slide menu uk-width-auto">
                                        <div className="uk-flex">
                                            <a className="uk-button uk-button-danger uk-padding-small uk-padding-remove-top uk-padding-remove-bottom"
                                                onClick={() => this.handlerDeleteCartItem(l, key)}>
                                                <span uk-icon="icon: trash"></span>
                                                <p className="uk-margin-remove">Remove Item</p>  
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
    handlerCheckout(){
        const obj = Functions.getListBeforePreview(this.state.list);
        if(obj.listPrev.length === 0){
            this.navigateToPreview(obj.list, obj.total);
        }else if(obj.listPrev.length > 0){
            if(obj.list.length > 0){
                UIkit.modal.confirm('You have some device has exchange not accepted, do you want to continue!').then(() => {
                    this.navigateToPreview(obj.list, obj.total);
                }, () => {
                    this.setState({listErr: obj.listPrev, list: obj.originalList});
                })
            }else{
                UIkit.modal.alert('Your Cart has error, please make proposal !!!')
                .then(() => {
                    this.setState({listErr: obj.listPrev, list: obj.originalList});
                })
            }
        }
    }
    navigateToPreview(list, total){
        localStorage.setItem('listPreview', JSON.stringify(list));
        localStorage.setItem('listPreviewTotal', total);
        setTimeout(() => {
            this.props.history.push('/preview');
        }, 500);
    }
    render(){
        return (
            <div>
                <Modal isShow={this.state.isModalSaleProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalSaleProposal: false})}>
                    <SaleProposal onSuccess={() => {
                        this.setState({isModalSaleProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalEditSaleProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalEditSaleProposal: false})}>
                    <SaleEditProposal onSuccess={() => {
                        this.setState({isModalEditSaleProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalExchangeProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalExchangeProposal: false})}>
                    <ExchangeProposal onSuccess={() => {
                        this.setState({isModalExchangeProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <Modal isShow={this.state.isModalEditExchangeProposal}
                    selectedCart={this.state.selectedCart}
                    socket={this.props.socket}
                    onHidden={() => this.setState({isModalEditExchangeProposal: false})}>
                    <ExchangeEditProposal onSuccess={() => {
                        this.setState({isModalEditExchangeProposal: false, selectedCart: {}}, () => {
                            this.refreshListCart();
                        })}
                    }/>
                </Modal>
                <div>
                    <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                        <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">My cart</div>
                    </div>
                    <div className="uk-inline uk-width-1-1 uk-margin">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        {
                            this.state.list.length > 0
                            ? this.renderCartList()
                            : <div className="uk-placeholder uk-text-center">There is no item in cart.</div>
                        }
                    </div>
                    
                    <div className="uk-padding">
                        <div className="uk-flex uk-flex-center uk-flex-between">
                            <a className="uk-padding-remove uk-width-1-1 uk-button uk-button-default uk-text-bold uk-text-emphasis"
                                onClick={() => this.props.history.push('/')}>Continue shopping</a>
                            {
                                this.state.list.length > 0 && !this.props.anonymous
                                ?
                                <a onClick={this.handlerCheckout.bind(this)}
                                    className="uk-margin-xsmall-left uk-padding-remove uk-width-1-1 uk-button uk-button-secondary uk-text-bold">Checkout</a>
                                : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectCart = connect(mapStateToProps, mapDispatchToProps)(withStorage(Cart));

export default ConnectCart;