import React from "react";
import { Button } from "../../../components/common";
import withStorage from "../../../hoc/storage";
import DeviceModel from "../../../models/device";
import TopDevice from "./topDevice";
import { SERVER_API, SERVER_DOMAIN } from "../../../config";
import ReactPaginate from 'react-paginate';
class AccountDevice extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            list: [],
            offset: 0,
            total: 0
        }
        this.pageDisplay = 4;
    }
    componentDidMount(){
        this.handlerApiListDevice()
    }
    handlerApiListDevice(){
        DeviceModel.getListMyDevice({email:this.props.auth, offset:this.state.offset, limit:this.pageDisplay})
        .then(result => {
            this.setState({list: result.list, total:Math.ceil(result.total / this.pageDisplay)});
        })
    }
    handlePageClick = data => {
        let selected = data.selected;
        let offset = Math.ceil(selected * this.pageDisplay);
    
        this.setState({ offset: offset, currentPage: selected }, () => {
            this.handlerApiListDevice();
        });
    };
    renderList(){
        return (
            <div>
                 <div className="uk-grid " uk-grid="true">
            {
                this.state.list.map((item, key) => {
                    return (
                        <div key={key} className="uk-width-1-4@l uk-width-1-3@m uk-width-1-2@s">
                            <div className="uk-card uk-card-small uk-card-body uk-inline">
                                <div className="uk-transition-toggle">
                                    <div className="uk-flex-center">
                                        {
                                            item.thumb
                                            ?
                                            <img className="uk-img" src={SERVER_DOMAIN+'devices/'+item.thumb} uk-img="true"/>
                                            :
                                            <img src={SERVER_DOMAIN+'no-image.png'}/>
                                        }
                                    </div>
                                    <h4 className="uk-text-truncate">{item.model_name}</h4>
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Brand</div>
                                                <div>{item.brand_name}</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Original Price</div>
                                                <div>{accounting.formatMoney(item.price)}</div>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-flex-between">
                                                <div className="uk-text-bold">Condition</div>
                                                <div>{item.condition} %</div>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-overlay uk-padding-remove uk-overlay-primary uk-position-top">
                                        <div className="uk-width-1-1 uk-padding-small">
                                            {
                                                item.available_id
                                                ?
                                                <a className="uk-link-reset" onClick={() => {
                                                    this.props.history.push('/myaccount/device/'+item.id+'/available/edit/'+item.available_id);
                                                }}>Edit Available</a>
                                                :
                                                <a className="uk-link-reset" onClick={() => {
                                                    this.props.history.push('/myaccount/device/'+item.id+'/available/add');
                                                }}>Make Available</a>
                                            }
                                        </div>
                                    </div>
                                    <div className="uk-card-footer uk-padding-remove">
                                        <div className="uk-grid uk-grid-collapse" uk-grid="true">
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-primary uk-width-1-1 uk-padding-remove"
                                                    onClick={() => this.props.history.push('/myaccount/device/edit/'+item.id)}>
                                                    <span uk-icon="icon: pencil"/>
                                                </button>
                                            </div>
                                            <div className="uk-width-1-2">
                                                <button className="uk-button uk-button-secondary uk-width-1-1 uk-padding-remove"
                                                    onClick={() => this.props.history.push('/myaccount/device/images/'+item.id)}>
                                                    <span uk-icon="icon: image"/>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })
            }
           
            </div>
            {
                this.state.list.length > 0
                ?
                <div className="uk-margin">
                    <ReactPaginate
                    previousLabel={'Previous'}
                    nextLabel={'Next'}
                    breakLabel={'...'}
                    breakClassName={'break-me'}
                    pageCount={this.state.total}
                    onPageChange={this.handlePageClick}
                    containerClassName={'pagination uk-flex uk-flex-center uk-padding-remove'}
                    subContainerClassName={'pages pagination'}
                    activeClassName={'active'}
                    />
                </div>
                :
                null
            }
            </div>
           )
    }
    render(){
        return (
            <div>
                <TopDevice/>
                <div className="uk-container">
                    <div className="uk-flex uk-flex-between uk-flex-middle uk-height-small">
                        <span className="uk-text-small uk-text-emphasis">SALES SUMARY</span>
                        <span><i className="uk-text-muted fa fa-refresh" aria-hidden="true"></i></span>
                    </div>
                    <div className="uk-flex uk-flex-between uk-flex-middle uk-height-small">
                        <span className="uk-text-small uk-text-bold">Today sold</span>
                        <span className="uk-text-small uk-text-muted uk-text-bold">23</span>
                    </div>
                    <div className="uk-flex uk-flex-between uk-flex-middle uk-height-small">
                        <span className="uk-text-small uk-text-bold">Weekend sales</span>
                        <span className="uk-text-small uk-text-muted uk-text-bold">87</span>
                    </div>
                    <div className="uk-flex uk-flex-between uk-flex-middle uk-height-small">
                        <span className="uk-text-small uk-text-bold">Total sold</span>
                        <span className="uk-text-small uk-text-muted uk-text-bold">237</span>
                    </div>
                </div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Device List</div>
                </div>
                <div className="uk-margin-top uk-padding-small">
                    {
                        this.state.list.length > 0
                        ? this.renderList()
                        : <div className="uk-placeholder uk-text-center">There is no device.</div>
                    }
                </div>
                <div className="uk-margin-bottom uk-container">
                    <Button className="uk-width-1-1" type="button" color="secondary" onClick={() => this.props.history.push('/myaccount/device/add')}>
                        Add Device
                    </Button>
                </div>
            </div>
        )
    }
}

export default withStorage(AccountDevice);