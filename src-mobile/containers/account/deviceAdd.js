import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import axios from "axios";
import { Form, Field } from 'react-final-form';
import { InputText, InputNumber, InputRange, Cover, Spinner, Button, Alert } from "../../../components/common";
import { SERVER_API } from "../../../config";

class AccountDeviceAdd extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            step: 0,
            isServerError: false,
            imei: '',
            condition: 100,
            category: '',
            brand: '',
            model: '',
            color: '',
            capacity: '',
            ram: ''
        }
        this.form = null;
        this.id_created = -1;
    }
    componentWillUnmount(){
        this.form = null;
        this.id_created = -1;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            if(!this.state.step){
                this.handlerApiCheckImei(values)
                .then(result => {
                    this.setState({step: 1,
                        imei: values.imei,
                        category: result.category_name,
                        brand: result.brand_name,
                        model: result.model_name,
                        color: result.color_name,
                        capacity: result.capacity_name,
                        ram: result.ram_name
                    }, () => {
                        resolve();
                    });
                })
                .catch(error => {
                    if(error.status === 400)
                        this.setState({isServerError: true});
                    resolve();
                })
            }else{
                this.handlerApiAddDevice(values)
                .then(result => {
                    this.id_created = result;
                    this.props.history.push('/myaccount/device/images/'+this.id_created);
                    UIkit.notification({message: 'Add Device Successfully, now please add images !', status: 'primary', pos: 'top-left'});
                    resolve();
                })
                .catch(error => {
                    resolve();
                })
            }
        })
    }
    handlerApiCheckImei(values){
        return axios.post(SERVER_API+'imei/check', {imei: values.imei});
    }
    handlerApiAddDevice(values){
        return axios.post(SERVER_API+'device/add', {imei: values.imei, email: this.props.auth, condition: values.condition, price: values.price});
    }
    renderStep(){
        let renderFields = null;
        if(this.state.step === 1){
            renderFields = (
                <React.Fragment>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                        <Field name="imei">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="imei">Imei | Serial Number</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <InputText {...input} type="text" placeholder="Your Imei | Serial Number"
                                        disabled={this.state.step} id="imei"/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2 uk-visible@l"/>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="category">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                                <InputText {...input} type="text" placeholder="Category" id="category"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="brand">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                                <InputText {...input} type="text" placeholder="Brand" id="brand"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="model">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                                <InputText {...input} type="text" placeholder="Model"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="color">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="color">Color</label>
                                <InputText {...input} type="text" placeholder="Color"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="capacity">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="capacity">Capacity</label>
                                <InputText {...input} type="text" placeholder="Capacity"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="ram">
                            {({ input }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="ram">Ram</label>
                                <InputText {...input} type="text" placeholder="Ram"
                                    disabled/>
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="price">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="price">Original Price</label>
                                <InputNumber {...input} placeholder="Price"/>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="condition">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column uk-margin">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="condition">Condition</label>
                                <InputRange {...input} onUpdate={(value) => {
                                    input.onChange(value)}
                                }/>
                            </div>
                            )}
                        </Field>
                    </div>
                </React.Fragment>
            )
        }else{
            renderFields = (
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="imei">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="imei">Imei | Serial Number</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Your Imei | Serial Number"
                                    disabled={this.state.step} id="imei"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            )
        }
        return (
            <div>
                <Form
                    initialValues={{
                        imei: this.state.imei,
                        condition: this.state.condition,
                        category: this.state.category,
                        brand: this.state.brand,
                        model: this.state.model,
                        color: this.state.color,
                        capacity: this.state.capacity,
                        ram: this.state.ram
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(!values.imei)
                                errors.imei = "Imei must be required";
                            else if(values.imei.length < 6)
                                errors.imei = "Imei must > 6 characters";
                            else if(values.imei.length > 50)
                                errors.imei = "Imei must < 50 characters";

                            if(this.state.step == 1){
                                if(!values.price)
                                    errors.price = "Price must be required";
                                else if(is.not.number(Number(values.price)))
                                    errors.price = "Price is not a number";
                                else if(Number(values.price) <= 0)
                                    errors.price = "Price must be larger than 0";
                            }
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                {this.state.isServerError && <Alert message={"IMEI checking failed !!!"}
                                    onBeforeHide={() => this.setState({isServerError: false})}/>}
                                <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                    {renderFields}
                                    <div className="uk-width-1-1 uk-margin uk-margin-remove-top">
                                        {
                                            !this.state.step
                                            ?
                                            <Button className="uk-width-auto" type="button" color="default"
                                                onClick={() => this.props.history.push('/account/device')}>
                                                Back to list
                                            </Button>
                                            :
                                            <Button className="uk-width-auto" type="button" color="default"
                                                onClick={() => this.setState({step: 0, imei: ''})}>
                                                Edit Imei
                                            </Button>
                                        }
                                        <Button className="uk-margin-xsmall-left uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                            {this.state.step ? 'Accept': 'Check Imei'}
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Add Device</div>
                </div>
                <div className="uk-container">
                    {this.renderStep()}
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(AccountDeviceAdd));