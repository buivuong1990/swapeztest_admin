import React from "react";
import { Cover, Spinner } from "../../../components/common";

import UserModel from "../../../models/user";

import withStorage from "../../../hoc/storage";
import { SERVER_DOMAIN } from "../../../config";
import TopDevice from "./topDevice";
import Functions from "../../../functions";


class ReceivedProposal extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            list: []
        }
        this.renderItem = this.renderItem.bind(this);
        this.observer = null;
    }
    componentDidMount(){
        Functions.socketReceivedProposal(this.props.socket)
        .subscribe((observer) => {
            this.observer = observer;
            this.refreshList();
        })
        this.refreshList();
    }
    componentWillUnmount(){
        if(this.observer)
            this.observer.complete();
    }
    refreshList(){
        this.setState({isLoading: true});
        UserModel.getListReceivedProposal({email: this.props.auth})
        .then((result) => {
            this.setState({list: result, isLoading: false});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    renderByType(l){
        if(Number(l.proposal_type) === 1){
            return (
                <ul className="uk-list">
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Proposal Price</div>
                            <div>{accounting.formatMoney(l.sale_proposal_price)}</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Type</div>
                            <div className="uk-text-capitalize">Sale</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Status</div>
                            <div className="uk-text-capitalize">{l.proposal_status}</div>
                        </div>
                    </li>
                </ul>
            )
        }else if(Number(l.proposal_type === 2)){
            return (
                <ul className="uk-list">
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Proposal Price</div>
                            <div>{accounting.formatMoney(l.exchange_proposal_price)}</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Type</div>
                            <div className="uk-text-capitalize">Exchange</div>
                        </div>
                    </li>
                    <li>
                        <div className="uk-flex uk-flex-between uk-width-medium">
                            <div className="uk-text-bold">Status</div>
                            <div className="uk-text-capitalize">{l.proposal_status}</div>
                        </div>
                    </li>
                </ul>
            )
        }
    }
    renderItem(){
        return this.state.list.map((l, key) => {
            return (
                <div key={key} className="uk-width-1-3@l uk-width-1-2@m uk-width-1-1@s uk-cursor"
                    onClick={() => {
                        if(Number(l.proposal_type) === 1)
                            this.props.history.push('/myaccount/received_proposal/sale/'+l.id)
                        else if(Number(l.proposal_type) === 2)
                            this.props.history.push('/myaccount/received_proposal/exchange/'+l.id)
                    }}>
                    <div className="uk-card uk-card-small uk-card-body uk-inline">
                        <div className="uk-grid" uk-grid="true">
                            <div className="uk-width-1-3 uk-flex uk-flex-middle">
                                {
                                    l.thumb 
                                    ?
                                    <a>
                                        <img className="uk-img uk-cursor" src={SERVER_DOMAIN+'devices/'+l.thumb} uk-img="true"/>
                                    </a>
                                    :
                                    <a>
                                        <img src={SERVER_DOMAIN+'no-image.png'}/>
                                    </a>
                                }
                            </div>
                            <div className="uk-width-2-3">
                                <h4 className="uk-cursor uk-text-truncate">
                                    {l.device_name}
                                </h4>
                                {
                                    this.renderByType(l)
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    }
    render(){
        return (
            <div>
                <TopDevice/>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin-bottom">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Received Proposals</div>
                </div>
                <div className="uk-container">
                    <div className="uk-inline uk-width-1-1">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
                        {
                            this.state.list.length == 0
                            ? <div className="uk-placeholder uk-text-center">There is no items.</div>
                            : null
                        }
                        <div className="uk-grid" uk-grid="true">
                            {this.renderItem()}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStorage(ReceivedProposal);