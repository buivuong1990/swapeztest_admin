import React from "react";
import UserModel from "../../../models/user";
import { Cover, Spinner } from "../../../components/common";
import withStorage from "../../../hoc/storage";
class Order extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: true,
            list: []
        }
        this.table = null;
    }
    refreshList() {
        this.setState({ isLoading: true });
        UserModel.getOrderList({ email: this.props.auth })
        .then((list) => {
            this.setState({ isLoading: false, list: list }, () => {
                if (this.state.list.length > 0)
                    this.handlerTabulator();
            });
        })
        .catch(() => {
            this.setState({ isLoading: true });
        })
    }
    componentDidMount(){
        this.refreshList();
    }
    componentWillUnmount() {
        this.table = null;
    }
    handlerTabulator(){
        this.table = new Tabulator(this.order, {
            data: this.state.list,
            height:"", 
            layout: "fitDataFill",
            pagination: "local",
            paginationSize: 6,
            columns:[
                {title:"No", field:"number", width:150, headerFilter: true, headerFilterPlaceholder: "Search number" },
                {title:"Total", field:"total", width:150, formatter: (cell) => {
                    var value = cell.getValue();
                    return accounting.formatMoney(value);
                }},
                {title:"Created at", field:"created_at", width:150, headerFilter: true, headerFilterPlaceholder: "Search creared at"},
            ]
        });
    }
    render(){
        return (
            <div className="uk-container uk-margin-top">
                <div className="uk-inline uk-width-1-1">
                    <h3 className="uk-margin-remove-top">Order List</h3>
                    {this.state.isLoading && <Cover />}
                    {this.state.isLoading && <Spinner />}
                    {
                        this.state.list.length > 0
                        ?
                        <div ref={instance => this.order = instance} ></div>
                        :
                        <div className="uk-placeholder uk-text-center">There is no items.</div>
                    }
                </div>
            </div>
        )
    }
}

export default withStorage(Order);