import React from "react";
import { Cover, Spinner, InputNumber } from "../../../components/common";

import UserModel from "../../../models/user";

import withStorage from "../../../hoc/storage";
import { SERVER_DOMAIN } from "../../../config";

import Functions from "../../../functions";

class ReceivedProposalSale extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            detail: {
            },
            sale_price: '',
            isLoading: false
        }
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.available_id = this.props.match.params.available_id;
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        UserModel.getDetailReceivedProposal({email: this.props.auth, id: this.id})
        .then((result) => {
            this.setState({isLoading: false, detail: result, sale_price: result.sale_proposal_price});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerSubmit(){
        if(is.empty(this.state.sale_price))
            UIkit.notification({message: 'Must Choose Price', status: 'danger', pos: 'top-left'});
        else{
            this.setState({isLoading: true});
            UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
                sale_proposal_price: this.state.sale_price, exchange_proposal_device: null, exchange_proposal_price: null,
                id: this.id, status: 'submitted'})
            .then(() => {
                UIkit.notification({message: 'Reply Proposal Successful', status: 'primary', pos: 'top-left'});
                Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                this.props.history.push('/account/received_proposal');
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }
    }
    handlerCancel(){
        UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
            sale_proposal_price: this.state.detail.sale_proposal_price, exchange_proposal_device: null, exchange_proposal_price: null,
            id: this.id, status: 'cancelled'})
        .then(() => {
            UIkit.notification({message: 'Cancel Proposal Successful', status: 'primary', pos: 'top-left'});
            Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
            this.props.history.push('/account/received_proposal');
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerAccept(){
        UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 1,
            sale_proposal_price: this.state.detail.sale_proposal_price, exchange_proposal_device: null, exchange_proposal_price: null,
            id: this.id, status: 'accepted'})
        .then(() => {
            UIkit.notification({message: 'Accept Proposal Successful', status: 'primary', pos: 'top-left'});
            Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
            this.props.history.push('/account/received_proposal');
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Received Proposal For {this.state.detail.device_name}</div>
                </div>
                <div className="uk-container uk-margin-large-top">
                    <div className="uk-position-relative uk-margin-large-bottom">
                        <div className="uk-grid uk-flex uk-flex-top" uk-grid="true">
                            {this.state.isLoading && <Cover/>}
                            {this.state.isLoading && <Spinner/>}
                            <div className="uk-width-1-3@l uk-width-1-2@m uk-with-1-1@s">
                            {
                                this.state.detail.thumb
                                ?
                                <div className="uk-flex uk-flex-center">
                                    <div>
                                        <img className="uk-img uk-cursor" src={SERVER_DOMAIN+'devices/'+this.state.detail.thumb} uk-img="true"
                                            style={{maxHeight: '290px'}}/>
                                    </div>
                                </div>
                                :
                                <div className="uk-flex uk-flex-center">
                                    <div>
                                        <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '290px'}}/>
                                    </div>
                                </div>
                            }
                            </div>
                            <div className="uk-width-2-3@l uk-width-1-2@m uk-with-1-1@s">
                                <h2>{this.state.detail.device_name}</h2>
                                <div className="uk-grid uk-margin" uk-grid="true">
                                    <div className="uk-width-1-1">
                                        <ul className="uk-list">
                                            <li>
                                                <div className="uk-flex uk-flex-between">
                                                    <div className="uk-text-bold">Brand:</div>
                                                    <span>{this.state.detail.brand_name}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="uk-flex uk-flex-between">
                                                    <div className="uk-text-bold">Color:</div>
                                                    <span>{this.state.detail.color_name}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div className="uk-flex uk-flex-between">
                                                    <div className="uk-text-bold">Capacity:</div>
                                                    <span>{this.state.detail.capacity_name}</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div className="uk-flex uk-flex-column uk-margin uk-width-medium">
                                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="proposal_sale_price">Proposal Sale Price</label>
                                            <InputNumber value={this.state.sale_price} icon={"credit-card"}
                                                onChange={event => this.setState({sale_price: event.target.value})}/>
                                        </div>
                                        <div>
                                            {
                                                this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                                ?
                                                <button className="uk-button uk-button-default"
                                                    onClick={this.handlerCancel.bind(this)}>Cancel</button>
                                                : null
                                            }
                                            {
                                                this.state.detail.proposal_status !== 'accepted'
                                                ?
                                                <button className="uk-button uk-button-secondary uk-margin-xsmall-left"
                                                    onClick={this.handlerSubmit.bind(this)}>Reply</button>
                                                : null
                                            }
                                            {
                                                this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                                ?
                                                <button className="uk-button uk-button-primary uk-margin-xsmall-left"
                                                    onClick={this.handlerAccept.bind(this)}>Accept</button>
                                                : null
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default withStorage(ReceivedProposalSale);