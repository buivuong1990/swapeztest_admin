import React from "react";
import { Select2, Cover, Spinner, Button,InputText,Checkbox} from "../../../components/common";
import axios from "axios";
import { Form, Field } from 'react-final-form';
import UserModel from "../../../models/user";
import DeviceModel from "../../../models/device";
import { SERVER_API } from "../../../config";
import withStorage from "../../../hoc/storage";
import { SERVER_DOMAIN } from "../../../config";

import Functions from "../../../functions";

class ReceivedProposalExchange extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmitProduct = this.handlerSubmitProduct.bind(this);
        this.handlerApiCategory = this.handlerApiCategory.bind(this);
        this.handlerChangeParentModel = this.handlerChangeParentModel.bind(this);
        this.state = {
            detail: {},
            exchange_price: '',
            device: {
                thumb: null
            },
            isLoading: false,
            mod: 0,
            recommend_device: null
        }
        this.form = null;
        this.category = 0;
        this.brand = 0;
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.available_id = this.props.match.params.available_id;

        this.setState({isLoading: true});
    }
    componentWillUnmount(){
        this.form = null;
        this.category = 0;
        this.brand = 0; 
    }
    handlerSubmitProduct(values){
        axios.get(SERVER_API+'model/name/'+values.model)
        .then((result) => {
            this.setState({isLoading:true},()=>{
                const deviceRecommend = JSON.stringify({category: result.category_name, category_id: result.category_id, brand: result.brand_name, brand_id: result.brand_id,
                   model: result.model_name, model_id: result.id});
                DeviceModel.recommendDevice({id:this.id, recommend_device: deviceRecommend})
                .then(()=>{
                    this.setState({isLoading:false, recommend_device: JSON.parse(deviceRecommend)});
                    Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                })
                .catch(() => {
                    this.setState({isLoading: false});
                })
            });
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerApiCategory(){
        this.categoryRef.beforeRefresh();
        axios.get(SERVER_API+'category/list')
        .then(result => {
            this.categoryRef.refreshList(result);
            if(is.not.empty(this.state.recommend_device) && this.state.recommend_device){
                this.categoryRef.triggerSelect(this.state.recommend_device.category_id);
            }
        })
        .catch(error => {
            this.categoryRef.afterRefresh();
        })
    }
    handlerApiBrand(){
        this.brandRef.beforeRefresh();
        axios.get(SERVER_API+'brand/list')
        .then(result => {
            this.brandRef.refreshList(result);
            if(is.not.empty(this.state.recommend_device) && this.state.recommend_device){
                this.brandRef.triggerSelect(this.state.recommend_device.brand_id);
            }
        })
        .catch(error => {
            this.brandRef.afterRefresh();
        })
    }
    handlerApiModel(){
        this.modelRef.beforeRefresh();
        axios.get(SERVER_API+'model/list/'+this.category+'/'+this.brand)
        .then(result => {
            this.modelRef.refreshList(result);
            if(is.not.empty(this.state.recommend_device) && this.state.recommend_device){
                this.modelRef.triggerSelect(this.state.recommend_device.model_id);
            }
        })
        .catch(error => {
            this.modelRef.afterRefresh();
        })
    }
    handlerChangeParentModel(fieldName, data, input, values){
        input.onChange(Number(data.id));
        this[fieldName] = Number(data.id);
        if(this.brand && this.category){
            this.values.model = '';
            this.modelRef.clearAll();
            this.handlerApiModel();
        }
    }
    handlerMode(event){
        const value = event.target.value;
        const checked = event.target.checked;
        if(!checked){
            this.setState({recommend_device:null});
                DeviceModel.recommendDeviceEmpty({id:this.id,recommend_device:this.state.recommend_device})
                .then(()=>{
                    Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                 })
                .catch(()=>{
                })
        }
        this.setState({[value]: checked}, () => {
            let mod = 0;
            const {recommend} = this.state;
            if(recommend) mod = 1;
            this.setState({mod: 0}, () => {
                this.setState({mod: mod}, () => {
                    if(this.state.mod == 1){
                        this.handlerApiCategory();
                        this.handlerApiBrand();
                    }
                })
            });
        });
    }  
    refreshDetail(){
        this.setState({isLoading: true});
        UserModel.getDetailReceivedProposal({email: this.props.auth, id: this.id})
        .then((result) => {
            const device = {
                id: result.exchange_device_id,
                name: result.exchange_device_name,
                thumb: result.exchange_device_thumb
            }
            this.setState({isLoading: false, detail: result, exchange_price: result.exchange_proposal_price, 
                device: device, recommend_device: JSON.parse(result.recommend_device)}, () => {
                this.initRecommendDevice();
            });
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    componentDidMount(){
        this.refreshDetail();
    }
    initRecommendDevice(){
        if(is.not.empty(this.state.recommend_device) && this.state.recommend_device){
            this.setState({mod: 1}, () => {
                this.handlerApiCategory();
                this.handlerApiBrand();
                this.handlerApiModel();
            });
        }
    }
    handlerSubmit(){
        if(is.empty(this.state.exchange_price))
            UIkit.notification({message: 'Must Choose Price', status: 'danger', pos: 'top-left'});
        else if(!is.number(Number(this.state.exchange_price)))
            UIkit.notification({message: 'Exchange Price Must Be Number', status: 'danger', pos: 'top-left'});
        else{
            this.setState({isLoading: true});
            UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 2,
                exchange_proposal_price: this.state.exchange_price, exchange_proposal_device: this.state.device.id, sale_proposal_price: null,
                id: this.id, status: 'submitted'})
            .then(() => {
                UIkit.notification({message: 'Reply Proposal Successful', status: 'primary', pos: 'top-left'});
                Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
                this.props.history.push('/account/received_proposal');
            })
            .catch(() => {
                this.setState({isLoading: false});
            })
        }
    }
    handlerCancel(){
        UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 2,
            exchange_proposal_price: this.state.detail.exchange_proposal_price, exchange_proposal_device: this.state.device.id, sale_proposal_price: null,
            id: this.id, status: 'cancelled'})
        .then(() => {
            UIkit.notification({message: 'Cancel Proposal Successful', status: 'primary', pos: 'top-left'});
            Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
            this.props.history.push('/account/received_proposal');
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    handlerAccept(){
        UserModel.editProposal({email: this.state.detail.user_id, cart_id: this.state.detail.cart_id, type: 2,
            exchange_proposal_price: this.state.detail.exchange_proposal_price, exchange_proposal_device: this.state.device.id, sale_proposal_price: null,
            id: this.id, status: 'accepted'})
        .then(() => {
            UIkit.notification({message: 'Accept Proposal Successful', status: 'primary', pos: 'top-left'});
            Functions.socketSendProposal(this.props.socket, this.state.detail.received_email);
            this.props.history.push('/account/received_proposal');
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    renderMode(){
        let renderFields = null;
        const {mod} = this.state;
        if(mod === 1)
        renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="category">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.categoryRef = instance} {...input} 
                                    placeholder="Choose Category" id="category"
                                    onChange={data => this.handlerChangeParentModel('category', data, input)}
                                    />
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="brand">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.brandRef = instance} {...input} 
                                    placeholder="Choose Brand" id="brand"
                                    onChange={data => this.handlerChangeParentModel('brand', data, input)}/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="model">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <Select2 ref={instance => this.modelRef = instance} {...input} 
                                    placeholder="Choose Model" id="model"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    onSubmit={this.handlerSubmitProduct}
                    validate={values => {
                        const errors = {};
                            if(!values.category)
                                errors.category = "Category must be choose";
                            if(!values.brand)
                                errors.brand = "Brand must be choose";
                            if(!values.model)
                                errors.model = "Model must be choose";
                        return errors;
                    }}
                    render={({ submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        this.values = values;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                <div className="uk-grid" uk-grid="true">
                                    {renderFields}
                                    {
                                        this.state.mod > 0
                                        ?
                                        <div className="uk-width-1-1 uk-margin">    
                                            <Button className="uk-width-auto uk-modal-close" type="submit" disabled={submitting || validating} color="primary">
                                                Choose
                                            </Button>
                                        </div>
                                        : null
                                    }   
                                </div>
                            </div>
                        </form>
                    }}/>
            </div>
        )
    }
    render(){
        return (
            <div>
                 <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Received Proposal For {this.state.detail.device_name}</div>
                </div>
                <div className="uk-position-relative uk-container">
                    <div className="uk-grid uk-flex uk-flex-top" uk-grid="true">
                        {this.state.isLoading && <Cover/>}
                        {this.state.isLoading && <Spinner/>}
      
                        <div className="uk-width-1-1">
                            <div className="uk-flex uk-flex-column">
                                {
                                    this.state.detail.thumb
                                    ?
                                    <div className="uk-flex uk-flex-center uk-margin-bottom">
                                        <div>
                                            <img className="uk-img uk-cursor" src={SERVER_DOMAIN+'devices/'+this.state.detail.thumb} uk-img="true"
                                                style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                    :
                                    <div className="uk-flex uk-flex-center uk-margin-bottom">
                                        <div>
                                            <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '120px'}}/>
                                        </div>
                                    </div>
                                }
                                <div className="uk-margin-top uk-margin-bottom uk-flex uk-flex-center uk-text-meta">Exchange With</div>
                                    <div className="uk-flex uk-flex-center uk-grid-divider uk-grid-small" uk-grid="true">
                                    {
                                        this.state.device && this.state.device.thumb
                                        ?
                                        <div className="uk-flex uk-flex-middle uk-flex-column">
                                            <div>
                                                <img src={SERVER_DOMAIN+'devices/'+this.state.device.thumb} style={{maxHeight: '120px'}}/>
                                            </div>
                                            <div className="uk-margin-top uk-text-bold">{this.state.device.name}</div>
                                        </div>
                                        :
                                        <div className="uk-flex uk-flex-middle uk-flex-column">
                                            <div>
                                            <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '120px'}}/>
                                            </div>
                                            <div className="uk-margin-top uk-text-bold">{this.state.device.name}</div>
                                        </div>
                                    }
                                    {
                                        this.state.recommend_device
                                        ?
                                        <div>
                                            <div className="uk-flex uk-flex-center">
                                                <img src={SERVER_DOMAIN+'no-image.png'} style={{maxHeight: '120px'}}/>
                                            </div>
                                            <div className="uk-margin-top uk-text-bold uk-text-center">{this.state.recommend_device.model}</div>
                                        </div>
                                        :
                                        null
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="uk-width-1-1">
                            <h2>{this.state.detail.device_name}</h2>
                            <div className="uk-grid uk-margin" uk-grid="true">
                                <div className="uk-width-1-1">
                                    <ul className="uk-list">
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Brand:</div>
                                                <span>{this.state.detail.brand_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Color:</div>
                                                <span>{this.state.detail.color_name}</span>
                                            </div>
                                        </li>
                                        <li>
                                            <div className="uk-flex uk-width-medium uk-flex-between">
                                                <div className="uk-text-bold">Capacity:</div>
                                                <span>{this.state.detail.capacity_name}</span>
                                            </div>
                                        </li>
                                    </ul>
                                    <div className="uk-flex uk-flex-column uk-margin uk-width-medium">
                                        <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="proposal_exchange_price">Proposal Exchange Price</label>
                                        <InputText value={this.state.exchange_price} icon={"credit-card"}
                                            onChange={event => this.setState({exchange_price: event.target.value})}/>
                                    </div>
                                    <div className="">
                                        {
                                            this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-default uk-margin-xsmall-right"
                                                onClick={this.handlerCancel.bind(this)}>Cancel</button>
                                            : null
                                        }
                                        {
                                            this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-secondary uk-margin-xsmall-right"
                                                onClick={this.handlerSubmit.bind(this)}>Reply</button>
                                            : null
                                        }
                                        {
                                            this.state.detail.proposal_status !== 'cancelled' && this.state.detail.proposal_status !== 'accepted'
                                            ?
                                            <button className="uk-button uk-button-primary"
                                                onClick={this.handlerAccept.bind(this)}>Accept</button>
                                            : null
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="uk-flex uk-margin-large">
                        <Checkbox label="Choose Product Recommend" value="recommend"
                            checked={this.state.mod}
                            onChange={this.handlerMode.bind(this)}
                            fontSize={'18px'}/>
                    </div>
                    {this.renderMode()}
                </div>
            </div>
        )
    }
}

export default withStorage(ReceivedProposalExchange);