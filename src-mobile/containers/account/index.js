import React from "react";
import withStorage from "../../../hoc/storage";
import { Route } from "react-router-dom";
import {SERVER_IMG} from "../../../config";

class Account extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <div className="uk-text-center uk-padding-large uk-background-blend-multiply uk-background-account uk-background-cover" 
                    style={{backgroundImage: 'url('+SERVER_IMG+'avatar.jpg)'}}>
                    <div className="uk-margin-small-bottom">
                        <img data-src={SERVER_IMG+"avatar.jpg"} alt="" className="uk-border-circle" width="80px" height="80px" uk-img="true"/>
                    </div>
                    <h2 className="uk-text-bold uk-text-white uk-text-large uk-margin-remove-top uk-margin-large-bottom">{this.props.auth}</h2>
                </div>
                
                {
                    this.props.routes.map((route, i) => (
                        <Route exact path={route.path} key={route.path}
                            render={
                                props => (
                                    <route.component {...props} socket={this.props.socket}/>
                                )
                        }/>
                    ))
                }
            </div>
        )
    }
}

export default withStorage(Account);