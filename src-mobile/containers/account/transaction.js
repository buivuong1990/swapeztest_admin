import React from "react";
import UserModel from "../../../models/user";
import { Cover, Spinner } from "../../../components/common";
import withStorage from "../../../hoc/storage";

class Transaction extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            list: []
        }
        this.table = null;

    }
    refreshList() {
        this.setState({ isLoading: true });
        UserModel.getTransactionList({ email: this.props.auth })
            .then((list) => {
                this.setState({ isLoading: false, list: list }, () => {
                    if (this.state.list.length > 0)
                        this.handlerTabulator();
                });
            })
            .catch(() => {
                this.setState({ isLoading: true });
            })
    }
    componentDidMount() {
        this.refreshList();
    }
    componentWillUnmount() {
        this.table = null;
    }

    handlerTabulator() {
        this.table = new Tabulator(this.transaction, {
            data: this.state.list,
            layout: "fitDataFill",
            pagination: "local",
            paginationSize: 6,
            columns: [
                { title: "No", field: "number", width: 180, sorter: "number", headerFilter: true, headerFilterPlaceholder: "Search number" },
                { title: "Device", field: "device_name", width: 180, headerFilter: true, headerFilterPlaceholder: "Search device" },
                { title: "Exchange Device", field: "exchange_device_name", width: 200, headerFilter: true, headerFilterPlaceholder: "Search exchange device" },
                {
                    title: "Type", field: "type", width: 180, headerFilter: true, headerFilterParams: { values: { "": "All", "1": "Sale", "2": "Exchange" } }, headerFilterPlaceholder: "Search type", editor: "select", editorParams: { values: { "1": "Sale", "2": "Exchange" } }, formatter: (cell) => {
                        var value = cell.getValue();
                        if (value == 1)
                            return "Sale"
                        else if (value == 2)
                            return "Exchange"
                    }
                },
                {
                    title: "Price", field: "price", editor: "number", width: 180, formatter: (cell) => {
                        var value = cell.getValue();
                        return accounting.formatMoney(value);
                    }
                },
                {
                    title: "Status", field: "status", width: 180, formatter: (cell) => {
                        var value = cell.getValue();
                        return "<span style='text-transform:uppercase; font-weight:bold;'>" + value + "</span>";
                    }, headerFilter: true, headerFilterParams: { values: { "": "All", "Created": "Created", "Cancelled": "Cancelled" } }, headerFilterPlaceholder: "Search status", editor: "select", editorParams: { values: { "created": "Created", "cancelled": "Cancelled" } }
                },
                { title: "Order.No", field: "order_number", width: 180, headerFilter: true, headerFilterPlaceholder: "Search order .No" },
                { title: "Created At", field: "created_at", sorter: "date", width: 160, headerFilter: true, headerFilterPlaceholder: "Search creared at", editor: true },
            ],
        });
    }
    render() {
        return (
            <div className="uk-container uk-margin-top">
                <div className="uk-inline uk-width-1-1">
                    <h3 className="uk-margin-remove-top">Transaction List</h3>
                    {this.state.isLoading && <Cover />}
                    {this.state.isLoading && <Spinner />}
                    {
                        this.state.list.length > 0
                            ?
                            <div ref={instance => this.transaction = instance} />
                            :
                            <div className="uk-placeholder uk-text-center">There is no items.</div>
                    }
                </div>
            </div>
        )
    }
}

export default withStorage(Transaction);