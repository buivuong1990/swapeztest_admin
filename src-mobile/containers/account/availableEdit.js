import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import axios from "axios";
import { Form, Field } from 'react-final-form';
import { InputText, Select2, Cover, Spinner, Button, Alert, Checkbox } from "../../../components/common";
import { SERVER_API } from "../../../config";

class AvailableEdit extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.handlerApiCategory = this.handlerApiCategory.bind(this);
        this.handlerChangeParentModel = this.handlerChangeParentModel.bind(this);
        this.state = {
            isServerError: false,
            sale: false,
            exchange: false,
            mod: 0,
            isLoading: false
        }
        this.form = null;
        this.id = 0;
        this.available_id = 0;
        this.category = 0;
        this.brand = 0;
        this.result = null;
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.available_id = this.props.match.params.available_id;
        this.setState({isLoading: true});
    }
    componentDidMount(){
        this.handlerApiDetailDevice()
        .then(result => {
            this.setState({
                isLoading: false,
                model_name: result.model_name
            }, () => {
                this.handlerApiDetailAvailable()
                .then(result => {
                    this.result = result;
                    if(result.type === 1)
                        this.saleRef.setCheck();
                    else if(result.type === 2)
                        this.exchangeRef.setCheck();
                    else if(result.type === 3){
                        this.saleRef.setCheck();
                        this.exchangeRef.setCheck();
                    }
                    this.setState({
                        sale_price: result.sale_price,
                        exchange_price: result.exchange_price,
                        mod: result.type
                    }, () => {
                        if(result.type > 1){
                            this.handlerApiCategory()
                            .then(() => {
                                this.setState({category: this.result.category_id}, () => {
                                    this.categoryRef.triggerSelect(this.result.category_id);
                                });
                                this.handlerApiBrand()
                                .then(() => {
                                    this.setState({brand: this.result.brand_id}, () => {
                                        this.brandRef.triggerSelect(this.result.brand_id);
                                    });
                                    this.handlerApiModel()
                                    .then(() => {
                                        this.setState({model: this.result.model_id}, () => {
                                            this.modelRef.triggerSelect(this.result.model_id);
                                        });
                                    })
                                })
                            })
                        }
                    });
                })
                .catch(error => {
                    if(error.status === 400){
                        UIkit.notification({message: 'Device Available Not Exists', status: 'error', pos: 'top-left'});
                        this.props.history.push('/account/device');
                    }else
                        this.props.history.push('/account/device');
                })
            });
        })
        .catch(error => {
            if(error.status === 400){
                UIkit.notification({message: 'Device Not Exists', status: 'error', pos: 'top-left'});
                this.props.history.push('/account/device');
            }else
                this.props.history.push('/account/device');
        })
    }
    componentWillUnmount(){
        this.form = null;
        this.id = 0;
        this.available_id = 0;
        this.category = 0;
        this.brand = 0;
        this.result = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.handlerApiEditAvailable(values)
            .then(result => {
                UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
                this.props.history.push('/account/device');
                resolve();
            })
            .catch(error => {
                resolve();
            })
        })
    }
    handlerMode(event){
        const value = event.target.value;
        const checked = event.target.checked;
        this.category = 0;
        this.brand = 0;
        this.form.reset();
        this.setState({[value]: checked}, () => {
            let mod = 0;
            const {sale, exchange} = this.state;
            if(sale && !exchange) mod = 1;
            else if(!sale && exchange) mod = 2;
            else if(sale && exchange) mod = 3;
            this.setState({mod: 0}, () => {
                this.setState({mod: mod}, () => {
                    if(this.state.mod > 1){
                        this.handlerApiCategory();
                        this.handlerApiBrand();
                    }
                })
            });
        });
    }
    handlerApiDetailDevice(){
        return axios.get(SERVER_API+'device/'+this.id);
    }
    handlerApiDetailAvailable(){
        return axios.get(SERVER_API+'device/available/'+this.available_id);
    }
    handlerApiCategory(){
        return new Promise((resolve, reject) => {
            this.categoryRef.beforeRefresh();
            axios.get(SERVER_API+'category/list')
            .then(result => {
                this.categoryRef.refreshList(result);
                resolve();
            })
            .catch(error => {
                this.categoryRef.afterRefresh();
            })
        })
    }
    handlerApiBrand(){
        return new Promise((resolve, reject) => {
            this.brandRef.beforeRefresh();
            axios.get(SERVER_API+'brand/list')
            .then(result => {
                this.brandRef.refreshList(result);
                resolve();
            })
            .catch(error => {
                this.brandRef.afterRefresh();
            })
        });
    }
    handlerApiModel(){
        return new Promise((resolve, reject) => {
            this.modelRef.beforeRefresh();
            axios.get(SERVER_API+'model/list/'+this.category+'/'+this.brand)
            .then(result => {
                this.modelRef.refreshList(result);
                resolve();
            })
            .catch(error => {
                this.modelRef.afterRefresh();
            })
        });
    }
    handlerApiEditAvailable(values){
        return axios.post(SERVER_API+'device/available/edit', {
            sale_price: values.sale_price,
            exchange_price: values.exchange_price,
            type: this.state.mod,
            device_id: this.id,
            model_id: values.model,
            id: this.available_id
        });
    }
    handlerChangeParentModel(fieldName, data, input){
        input.onChange(Number(data.id));
        this[fieldName] = Number(data.id);
        if(this.brand && this.category){
            this.modelRef.clearAll();
            this.handlerApiModel();
        }
    }
    renderMode(){
        let renderFields = null;
        const {mod} = this.state;
        if(mod === 1)
            renderFields = (
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="sale_price">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="sale_price">Sale Price</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Sale Price" id="sale_price"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            )
        else if(mod === 2)
            renderFields = (
                <React.Fragment>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                        <Field name="exchange_price">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="exchange_price">Exchange Price</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <InputText {...input} type="text" placeholder="Exchange Price" id="exchange_price"/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="category">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.categoryRef = instance} {...input} 
                                        placeholder="Choose Category" id="category"
                                        onChange={data => this.handlerChangeParentModel('category', data, input)}/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="brand">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.brandRef = instance} {...input} 
                                        placeholder="Choose Brand" id="brand"
                                        onChange={data => this.handlerChangeParentModel('brand', data, input)}/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="model">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.modelRef = instance} {...input} 
                                        placeholder="Choose Model" id="model"
                                        onChange={data => {
                                            input.onChange(Number(data.id))}
                                        }/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                </React.Fragment>
            )
        else if(mod === 3){
            renderFields = (
                <React.Fragment>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                        <Field name="sale_price">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="sale_price">Sale Price</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <InputText {...input} type="text" placeholder="Sale Price" id="sale_price"/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="exchange_price">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="exchange_price">Exchange Price</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <InputText {...input} type="text" placeholder="Exchange Price" id="exchange_price"/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="category">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.categoryRef = instance} {...input} 
                                        placeholder="Choose Category" id="category"
                                        onChange={data => this.handlerChangeParentModel('category', data, input)}/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="brand">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.brandRef = instance} {...input} 
                                        placeholder="Choose Brand" id="brand"
                                        onChange={data => this.handlerChangeParentModel('brand', data, input)}/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                        <Field name="model">
                            {({ input, meta }) => (
                            <div className="uk-flex uk-flex-column">
                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                                <div className="uk-inline">
                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                    <Select2 ref={instance => this.modelRef = instance} {...input} 
                                        placeholder="Choose Model" id="model"
                                        onChange={data => {
                                            input.onChange(Number(data.id))}
                                        }/>
                                </div>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            )}
                        </Field>
                    </div>
                </React.Fragment>
            )
        }
        return (
            <div>
                <div className="uk-flex uk-margin uk-margin-top">
                    <div>
                        <Checkbox label="Sale" value="sale" 
                            ref={instance => this.saleRef = instance}
                            onChange={this.handlerMode.bind(this)}
                            fontSize={'18px'}/>
                    </div>
                    <div className="uk-margin-large-left">
                        <Checkbox label="Exchange" value="exchange" 
                            ref={instance => this.exchangeRef = instance}
                            onChange={this.handlerMode.bind(this)}
                            fontSize={'18px'}/>
                    </div>
                </div>
                <Form
                    initialValues={{
                        sale_price: this.state.sale_price,
                        exchange_price: this.state.exchange_price
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(this.state.mod === 1){
                                if(!values.sale_price)
                                    errors.sale_price = "Sale Price must be required";
                                else if(is.not.number(Number(values.sale_price)))
                                    errors.sale_price = "Sale Price is not a number";
                                else if(Number(values.sale_price) <= 0)
                                    errors.sale_price = "Sale Price must be larger than 0";
                            }else if(this.state.mod === 2){
                                if(!values.exchange_price)
                                    errors.exchange_price = "Exchange Price must be required";
                                else if(is.not.number(Number(values.exchange_price)))
                                    errors.exchange_price = "Exchange Price is not a number";

                                if(!values.category)
                                    errors.category = "Category must be choose";
                                if(!values.brand)
                                    errors.brand = "Brand must be choose";
                                if(!values.model)
                                    errors.model = "Model must be choose";
                            }else if(this.state.mod === 3){
                                if(!values.sale_price)
                                    errors.sale_price = "Sale Price must be required";
                                else if(is.not.number(Number(values.sale_price)))
                                    errors.sale_price = "Sale Price is not a number";
                                else if(Number(values.sale_price) <= 0)
                                    errors.sale_price = "Sale Price must be larger than 0";
                                if(!values.exchange_price)
                                    errors.exchange_price = "Exchange Price must be required";
                                else if(is.not.number(Number(values.exchange_price)))
                                    errors.exchange_price = "Exchange Price is not a number";

                                if(!values.category)
                                    errors.category = "Category must be choose";
                                if(!values.brand)
                                    errors.brand = "Brand must be choose";
                                if(!values.model)
                                    errors.model = "Model must be choose";
                            }
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                {this.state.isServerError && <Alert message={"IMEI checking failed !!!"}
                                    onBeforeHide={() => this.setState({isServerError: false})}/>}
                                <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                    {renderFields}
                                    <div className="uk-width-1-1 uk-margin">    
                                        <Button className="uk-width-auto" type="button" color="default"
                                            onClick={() => this.props.history.push('/account/device')}>
                                            Back to list
                                        </Button>
                                        {
                                            this.state.mod > 0
                                            ?
                                            <Button className="uk-margin-xsmall-left uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                                Accept
                                            </Button>
                                            : null
                                        }
                                    </div>
                                </div>
                                <a onClick={() => {
                                    axios.delete(SERVER_API+'device/available', {data: {id: this.available_id}})
                                    .then(result => {
                                        UIkit.notification({message: 'Remove Available Successfully', status: 'primary', pos: 'top-left'});
                                        this.props.history.push('/account/device');
                                    })
                                }}><p>Remove Available </p></a>
                            </div>
                        </form>
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Edit Available For {this.state.model}</div>
                </div>
                <div className="uk-container">
                    {this.renderMode()}
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(AvailableEdit));