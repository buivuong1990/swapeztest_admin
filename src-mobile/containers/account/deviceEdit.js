import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import axios from "axios";
import { Form, Field } from 'react-final-form';
import { InputText, InputNumber, InputRange, Cover, Spinner, Button, Alert } from "../../../components/common";
import { SERVER_API } from "../../../config";

class AccountDeviceEdit extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isServerError: false,
            imei: '',
            condition: 100,
            category: '',
            brand: '',
            model: '',
            color: '',
            capacity: '',
            ram: '',
            isLoading: false
        }
        this.form = null;
        this.id = '';
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.setState({isLoading: true});
    }
    componentDidMount(){
        this.handlerApiDetailDevice()
        .then(result => {
            this.setState({
                isLoading: false,
                brand: result.brand_name,
                capacity: result.capacity_name,
                category: result.category_name,
                color: result.color_name,
                condition: result.condition,
                imei: result.imei,
                model: result.model_name,
                ram: result.ram_name,
                price: result.price
            });
        })
        .catch(error => {
            if(error.status === 400){
                UIkit.notification({message: 'Device Not Exists', status: 'error', pos: 'top-left'});
                this.props.history.push('/account/device');
            }else
                this.props.history.push('/account/device');
        })
    }
    componentWillUnmount(){
        this.form = null;
        this.id = '';
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.handlerApiEditDevice(values)
            .then(result => {
                UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
                this.props.history.push('/account/device');
                resolve();
            })
            .catch(error => {
                resolve();
            })
        })
    }
    handlerApiDetailDevice(){
        return axios.get(SERVER_API+'device/'+this.id);
    }
    handlerApiEditDevice(values){
        return axios.post(SERVER_API+'device/edit', {id: this.id, condition: values.condition, price: values.price});
    }
    renderStep(){
        const renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="imei">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="imei">Imei | Serial Number</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Your Imei | Serial Number"
                                    disabled={this.state.step} id="imei"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2 uk-visible@l"/>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="category">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                            <InputText {...input} type="text" placeholder="Category" id="category"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="brand">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                            <InputText {...input} type="text" placeholder="Brand" id="brand"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="model">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                            <InputText {...input} type="text" placeholder="Model"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="color">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="color">Color</label>
                            <InputText {...input} type="text" placeholder="Color"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="capacity">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="capacity">Capacity</label>
                            <InputText {...input} type="text" placeholder="Capacity"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="ram">
                        {({ input }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="ram">Ram</label>
                            <InputText {...input} type="text" placeholder="Ram"
                                disabled/>
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="price">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="price">Original Price</label>
                            <InputNumber {...input} placeholder="Price"/>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="condition">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column uk-margin">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="condition">Condition</label>
                            <InputRange {...input} onUpdate={(value) => {
                                input.onChange(value)}
                            }/>
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    initialValues={{
                        imei: this.state.imei,
                        condition: this.state.condition,
                        category: this.state.category,
                        brand: this.state.brand,
                        model: this.state.model,
                        color: this.state.color,
                        capacity: this.state.capacity,
                        ram: this.state.ram,
                        price: this.state.price
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(!values.imei)
                                errors.imei = "Imei must be required";
                            else if(values.imei.length < 6)
                                errors.imei = "Imei must > 6 characters";
                            else if(values.imei.length > 50)
                                errors.imei = "Imei must < 50 characters";

                            if(!values.price)
                                errors.price = "Price must be required";
                            else if(is.not.number(Number(values.price)))
                                errors.price = "Price is not a number";
                            else if(Number(values.price) <= 0)
                                errors.price = "Price must be larger than 0";
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting || this.state.isLoading && <Cover/>}
                                {submitting || this.state.isLoading && <Spinner/>}
                                {this.state.isServerError && <Alert message={"IMEI checking failed !!!"}
                                    onBeforeHide={() => this.setState({isServerError: false})}/>}
                                <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                    {renderFields}
                                    <div className="uk-width-1-1 uk-margin uk-margin-remove-top">
                                        <Button className="uk-width-auto" type="button" color="default"
                                            onClick={() => this.props.history.push('/account/device')}>
                                            Back to list
                                        </Button>
                                        <Button className="uk-margin-xsmall-left uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                            Accept
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle uk-margin">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Edit Device {this.state.model}</div>
                </div>
                <div className="uk-container">
                    {this.renderStep()}
                </div>
            </div>
        )
    }
}

export default withRouter(withStorage(AccountDeviceEdit));