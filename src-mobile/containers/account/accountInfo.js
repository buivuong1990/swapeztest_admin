import React from "react";
import {withRouter} from "react-router-dom";
import withStorage from "../../../hoc/storage";
import { Form, Field } from 'react-final-form';
import { InputText, Select2, Cover, Spinner, Button, Checkbox } from "../../../components/common";
import { COUNTRY_LIST } from "../../../config";
import UserModel from "../../../models/user";

class AccountInfo extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.renderBilling = this.renderBilling.bind(this);
        
        this.state = {
            full_name: '',
            country: 0,
            address1: '',
            address2: '',
            city: '',
            postal_code: '',
            isLoading: false
        }
        this.form = null;
    }
    componentDidMount(){
        this.setState({isLoading: true});
        UserModel.getUserDetail({email: this.props.auth})
        .then((user) => {
            const {full_name, country, address1, address2, city, postal_code} = user;
            this.setState({isLoading: false, full_name, country, address1, address2, city, postal_code});
        })
        .catch(() => {
            this.setState({isLoading: false});
        })
    }
    componentWillUnmount(){
        this.form = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            const {full_name, country, address1, address2, city, postal_code} = values;
            UserModel.editUser({email: this.props.auth, full_name, country, address1, address2, city, postal_code})
            .then(result => {
                UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
                resolve();
            })
            .catch(error => {
                resolve();
            })
        })
    }
  
    renderBilling(){
        const renderFields = (
            <React.Fragment>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin">
                    <Field name="full_name">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="full_name">Full Name</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Full Name"
                                    disabled={this.state.step} id="full_name"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="country">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="country">Country</label>
                            <div className="uk-inline">
                                <Select2 ref={instance => {
                                    if(is.existy(instance))
                                        instance.refreshList(COUNTRY_LIST);
                                }} {...input} 
                                    placeholder="Select Country" id="country"
                                    onChange={data => {
                                        input.onChange(Number(data.id))}
                                    }
                                />
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address1">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address1">Address 1</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Address 1"
                                    disabled={this.state.step} id="address1"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="address2">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="address2">Address 2</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Address 2"
                                    disabled={this.state.step} id="address2"/>
                                {
                                    meta.touched && meta.error &&
                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                        {meta.error || meta.submitError}
                                    </span>
                                }
                            </div>
                            
                        </div>
                        )}
                    </Field>
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="city">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="city">City</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="City"
                                    disabled={this.state.step} id="city"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>        
                </div>
                <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                    <Field name="postal_code">
                        {({ input, meta }) => (
                        <div className="uk-flex uk-flex-column">
                            <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="postal_code">Postal Code</label>
                            <div className="uk-inline">
                                <span className="uk-form-icon" uk-icon="icon: check"/>
                                <InputText {...input} type="text" placeholder="Postal Code"
                                disabled={this.state.step} id="postal_code"/>
                            </div>
                            {
                                meta.touched && meta.error &&
                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                    {meta.error || meta.submitError}
                                </span>
                            }
                        </div>
                        )}
                    </Field>
                </div>
            </React.Fragment>
        )
        return (
            <div>
                <Form
                    initialValues={{
                        full_name: this.state.full_name,
                        country: this.state.country,
                        address1: this.state.address1,
                        address2: this.state.address2,
                        city: this.state.city,
                        postal_code: this.state.postal_code
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(!values.full_name)
                                errors.full_name = "Full Name must be required";

                            if(!values.country)
                                errors.country = "Country must be required";

                            if(!values.address1)
                                errors.address1 = "Address 1 must be required";

                            if(!values.city)
                                errors.city = "City must be required";

                            if(!values.postal_code)
                                errors.postal_code = "Postal Code must be required";
                                
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        return (
                            <React.Fragment>
                                <form onSubmit={handleSubmit}>
                                    <div className="uk-inline uk-width-1-1">
                                        {submitting || this.state.isLoading && <Cover/>}
                                        {submitting || this.state.isLoading && <Spinner/>}
                                        <div className="uk-grid uk-margin-xsmall-top" uk-grid="true">
                                            {renderFields}
                                        </div>
                                    </div>

                                    <div className="uk-width-1-1 uk-margin">
                                        <Button className="uk-width-auto" type="submit" disabled={submitting || validating} color="primary">
                                            Update
                                        </Button>
                                    </div>
                                </form>
                            </React.Fragment>
                        )
                    }}
                >
                </Form>
            </div>
        )
    }
    render(){
        return (
            <div className="uk-container uk-margin-top">
                <h4>Account Information</h4>
                {this.renderBilling()}
            </div>
        )
    }
}

export default withRouter(withStorage(AccountInfo));