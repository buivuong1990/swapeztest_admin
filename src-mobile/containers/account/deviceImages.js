import React from "react";
import axios from "axios";
import { SERVER_API } from "../../../config";
import {ImageUpload} from "../../../components/common";

class AccountDeviceImages extends React.Component{
    constructor(props){
        super(props);
        this.id = '';
        this.imageUpload = null;
        this.state = {
            isLoading: true,
            model: '',
            list: []
        }
        this.handlerServerUpload = this.handlerServerUpload.bind(this);
        this.handlerApiChecked = this.handlerApiChecked.bind(this);
        this.handlerApiDelete = this.handlerApiDelete.bind(this);
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
        this.setState({isLoading: true});
    }
    componentDidMount(){
        this.handlerApiDetailDevice()
        .then(result => {
            this.setState({
                isLoading: false,
                model: result.model_name
            }, () => {
                this.handlerApiImages();
            });
        })
        .catch(error => {
            if(error.status === 400){
                UIkit.notification({message: 'Device Not Exists', status: 'error', pos: 'top-left'});
                this.props.history.push('/account/device');
            }else
                this.props.history.push('/account/device');
        })
    }
    componentWillUnmount(){
        this.id = '';
        this.imageUpload = null;
    }
    handlerApiDetailDevice(){
        return axios.get(SERVER_API+'device/'+this.id);
    }
    handlerApiImages(){
        this.imageUpload.beforeRefresh();
        axios.get(SERVER_API+'device/image/'+this.id)
        .then(result => {
            this.imageUpload.refreshList(result);
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiChecked(prevId, postId){
        this.imageUpload.beforeRefresh();
        axios.post(SERVER_API+'device/image/check', {prev_id: prevId, post_id: postId})
        .then(result => {
            this.handlerApiImages();
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiDelete(item){
        this.imageUpload.beforeRefresh();
        axios.delete(SERVER_API+'device/image', {data: {id: item.id}})
        .then(result => {
            this.handlerApiImages();
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiUploadImage(formData){
        return axios({
            method: 'post',
            url: SERVER_API+'device/image',
            data: formData
        })
    }
    handlerServerUpload(files){
        const formData = new FormData();
        formData.append('photo', files[0]);
        formData.append('id', this.id);
        this.handlerApiUploadImage(formData)
        .then(() => {
            this.handlerApiImages();
        })
        .catch(error => {
            if(error.status === 400)
                UIkit.notification({message: 'This Image Cannot Upload', status: 'error', pos: 'top-left'});
        })
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-text-uppercase uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Device Images</div>
                </div>
                <div className="uk-container uk-margin-top">
                    <ul className="uk-breadcrumb">
                        <li><a onClick={() => this.props.history.push('/account/device')}>List Device</a></li>
                        <li><span>{this.state.model}</span></li>
                    </ul>
                    <h3 className="uk-margin-remove-top uk-margin-large">Images For {this.state.model}</h3>
                    <ImageUpload maxFiles={5}
                        ref={instance => this.imageUpload = instance}
                        onSelectChecked={this.handlerApiChecked}
                        onDelete={this.handlerApiDelete}
                        onChange={this.handlerServerUpload}/>
                </div>
            </div>
        )
    }
}

export default AccountDeviceImages;