import React from "react";
import axios from "axios";
import { SERVER_API } from "../../../config";
import {Redirect} from "react-router";

import { Form, Field } from 'react-final-form';
import { InputText, InputPassword, Cover, Spinner, Button, Alert } from "../../../components/common";

import {connect} from "react-redux";
import {addCart} from "../../../src/containers/home/actions";

import withStorage from "../../../hoc/storage";

import UserModel from "../../../models/user";

class Login extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isRedirect: false,
            isServerError: false
        }
        this.resetForm = null;
    }
    componentWillUnmount(){
        this.resetForm = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.handlerApiLogin(values)
            .then(result => {
                this.handlerGlobalStorage(values);
            })
            .catch(error => {
                if(error.status === 400)
                    this.setState({isServerError: true});
                this.resetForm();
                resolve();
            })
        })
    }
    handlerApiLogin(values){
        return axios.post(SERVER_API+'user/login', {email: values.email, password: values.password});
    }
    handlerGlobalStorage(values){
        UserModel.syncCart({prev_email: this.props.auth, email: values.email})
        .then(() => {
            UIkit.notification({message: 'Login Successfully !', status: 'primary', pos: 'top-left'});
            localStorage.setItem('email', values.email);
            localStorage.setItem('anonymous', false);
            setTimeout(() => {
                this.props.app.setState({isMenu: false}, () => {
                    this.props.app.setState({isMenu: true}, () => {
                        this.props.history.push('/');
                    });
                });
            }, 500);
        })
        .catch(() => {
            UIkit.notification({message: 'Login Successfully !', status: 'primary', pos: 'top-left'});
            localStorage.setItem('email', values.email);
            localStorage.setItem('anonymous', false);
            setTimeout(() => {
                this.props.app.setState({isMenu: false}, () => {
                    this.props.app.setState({isMenu: true}, () => {
                        this.props.history.push('/');
                    });
                });
            }, 500);
        })
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Login</div>
                </div>
                <div className="uk-padding">
                    <div className="uk-flex uk-flex-center uk-margin-top uk-margin-bottom">
                        <i className="fa fa-user uk-icon-login"></i>
                    </div>
                    <Form
                        onSubmit={this.handlerSubmit}
                        validate={values => {
                                const errors = {};
                                if(!values.email)
                                    errors.email = "Email must be required";
                                else if(is.not.email(values.email))
                                    errors.email = "Invalid Email";
                                else if(values.email.length < 6)
                                    errors.email = "Email must > 6 characters";
                                else if(values.email.length > 50)
                                    errors.email = "Email must < 50 characters";
                                
                                if(!values.password)
                                    errors.password = "Password must be required";
                                else if(values.password.length < 6)
                                    errors.password = "Password must > 6 characters";
                                else if(values.password.length > 50)
                                    errors.password = "Password must < 50 characters";
                                return errors;
                            }
                        }
                        render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit} >
                                <div className="uk-inline uk-width-1-1">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    {this.state.isServerError && <Alert message={"Login Failed !!!"}
                                        onBeforeHide={() => this.setState({isServerError: false})}/>}
                                    <Field name="email">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column uk-margin">
                                            <div className="uk-inline uk-width-1-1">
                                                <span className="uk-form-icon" uk-icon="icon: mail"/>
                                                <InputText {...input} type="email" placeholder="Your Email"/>
                                            </div>
                                            {
                                                meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error || meta.submitError}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
                                    <Field name="password">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column uk-margin">
                                            <InputPassword {...input} placeholder="Your Password"/>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
                                    <div className="uk-flex uk-margin">
                                        <Button className="uk-width-1-1" type="submit" disabled={submitting || validating} color="primary">
                                            Login
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}
                    >
                    </Form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        count: state.home.count
    }
}

const mapDispatchToProps = {
    addCart
}

const ConnectLogin = connect(mapStateToProps, mapDispatchToProps)(withStorage(Login));

export default ConnectLogin;