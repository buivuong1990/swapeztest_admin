import React from "react";
import DeviceModel from "../../../models/device";
import withStorage from "../../../hoc/storage";
import Item from "../../../components/mobile/item";

class QuickCompare extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isLoading: false,
            detail: {},
            detailCompare: {}
        }
        this.next = 0;
        this.list = [];
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentWillUnmount(){
        this.next = 0;
        this.list = [];
    }
    componentDidMount(){
        this.refreshDetail();
    }
    refreshDetail(){
        this.setState({isLoading: true});
        DeviceModel.getDeviceCompare({id: this.id, email: this.props.auth})
        .then((result) => {
            this.list = result.listCompare;
            this.setState({detail: result.main, detailCompare: result.listCompare[this.next], isLoading: false});
        })
        .catch(() => this.setState({isLoading: false}))
    }
    handlerNext(){
        if(this.list.length > Number(this.next)+1){
            this.next++;
        }else this.next = 0;
        this.setState({detailCompare: this.list[this.next]});
    }
    render(){
        return (
            <div>
                <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                    <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Quick Compare</div>
                </div>
                <div className="uk-container uk-margin-top">
                    <div className="uk-grid-small" uk-grid="true">
                        <div className="uk-width-1-2">
                            <div className="uk-flex uk-flex-center uk-height-item@s uk-height-item@m">
                                <Item item={this.state.detail} mode="compare"
                                    onAddCart={this.props.onAddCart}/>
                            </div>
                            <div className="uk-text-center">
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.category_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.brand_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.color_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.capacity_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detail.ram_name}</div>
                            </div>
                        </div>
                        <div className="uk-width-1-2 uk-inline">
                            <div className="uk-position-top-right" style={{zIndex: 888}}>
                                <div className="uk-link-reset" 
                                    onClick={this.handlerNext.bind(this)}>
                                    <i className="fa fa-close fa-2x"/>
                                </div>
                            </div>
                            <div  className="uk-flex uk-flex-center uk-height-item@s uk-height-item@m">
                                <Item item={this.state.detailCompare} mode="compare"
                                    onAddCart={this.props.onAddCart}/>
                            </div>
                            <div className="uk-text-center">
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.category_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.brand_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.color_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.capacity_name}</div>
                                <div className="border-breadcrumb uk-padding-small uk-height-small">{this.state.detailCompare.ram_name}</div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*<div className="uk-text-center uk-padding-small"><a className="uk-text-underline uk-text-primary">VIEW MORE COMPARE</a></div>*/}
            </div>
        )
    }
}

export default withStorage(QuickCompare);