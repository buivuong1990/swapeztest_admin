import React from "react";
import {SERVER_IMG} from "../../../config";

class Available extends React.Component{
    componentDidMount(){
        var $element = $('#check-exchange');
        UIkit.toggle($element, {
            target: '#block-exchange'
        }).toggle();
    }
    render(){
        return (
            <div>
                <div className="uk-flex uk-flex-middle uk-padding-small">
                    <div>
                        <img data-src={SERVER_IMG+"images-1539244325906.png"} width="125px" height="" alt="" uk-img="true" />
                    </div>
                    <span className="uk-text-bold uk-margin-left">HTC One-64GB</span>
                </div>
                
                <form>
                    <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                        <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Please choose status option</div>
                    </div>
                    <div className="uk-margin uk-padding-small uk-child-width-1-3 uk-grid">
                        <label><input className="uk-checkbox uk-margin-xsmall-right" type="checkbox" checked/> Sale</label>
                        <label><input className="uk-checkbox uk-margin-xsmall-right" type="checkbox" id="check-exchange"/> Exchange</label>
                    </div>

                    <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                        <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Please choose sale price</div>
                    </div>
                    <div className="uk-padding-small">
                        <span className="uk-text-small uk-text-bold uk-text-meta">Add price here.</span>
                        <div className="uk-flex uk-flex-between uk-flex-middle uk-margin-top">
                            <input className="uk-input uk-margin-right" type="text" placeholder="Your price"/>
                            <select className="uk-select">
                                <option>USD</option>
                                <option>CAD</option>
                                <option>EUR</option>
                            </select>
                        </div>
                    </div>
                    <div id="block-exchange">
                        <div className="uk-box-shadow-medium uk-height-small uk-flex uk-flex-middle">
                            <div className="uk-padding-small uk-text-small uk-text-uppercase uk-text-emphasis uk-text-bold">Please select product you want to exchange</div>
                        </div>
                        <div className="uk-padding-small">
                            <span className="uk-text-small uk-text-bold uk-text-meta">Condition. All devices listed for sale on Swap-ez marketplace must meet our Criteria for Sale.</span>
                            <div className="uk-margin">
                                <div className="uk-form-controls">
                                    <select className="uk-select" id="form-stacked-select">
                                        <option>Choose Category</option>
                                        <option>Smart Phone</option>
                                        <option>Tablets</option>
                                        <option>Smart Watches</option>
                                    </select>
                                </div>
                            </div>
                            <div className="uk-margin">
                                <div className="uk-form-controls">
                                    <select className="uk-select" id="form-stacked-select">
                                        <option>Choose Brand</option>
                                        <option>Apple</option>
                                        <option>Saumsung</option>
                                        <option>HTC</option>
                                    </select>
                                </div>
                            </div>
                            <div className="uk-margin">
                                <div className="uk-form-controls">
                                    <select className="uk-select" id="form-stacked-select">
                                        <option>Choose Model</option>
                                        <option>Apple</option>
                                        <option>Saumsung</option>
                                        <option>HTC</option>
                                    </select>
                                </div>
                            </div>
                            <div className="uk-margin">
                                <div className="uk-form-controls">
                                    <select className="uk-select" id="form-stacked-select">
                                        <option>Choose Color</option>
                                        <option>Black</option>
                                        <option>White</option>
                                    </select>
                                </div>
                            </div>           
                            <div className="uk-margin">
                                <div className="uk-form-controls">
                                    <select className="uk-select" id="form-stacked-select">
                                        <option>Choose Capacity</option>
                                        <option>64GB</option>
                                        <option>128GB</option>
                                    </select>
                                </div>
                            </div>
                            <span className="uk-text-small uk-text-bold uk-text-meta">Add price here.</span>
                            <div className="uk-flex uk-flex-between uk-flex-middle uk-margin-top">
                                <input className="uk-input uk-margin-right" type="text" placeholder="Your price"/>
                                <select className="uk-select">
                                    <option>USD</option>
                                    <option>CAD</option>
                                    <option>EUR</option>
                                </select>
                            </div>
                        </div>
                    </div>
                   
                    <div className="uk-padding-small">
                        <div className="uk-flex uk-flex-center uk-flex-between">
                            <a className="uk-button uk-width-1-1 uk-button-default uk-text-bold uk-text-emphasis">Submit</a>
                            <a className="uk-margin-left uk-button uk-width-1-1 uk-button-secondary uk-text-bold" >Cancel</a>
                        </div>
                        <div className="uk-margin-top">
                            <span uk-toggle="target: #modal-remove" className="uk-margin-top uk-margin-remove">
                            <i className="fa fa-trash-o" aria-hidden="true"></i> Remove from available list</span>
                            <div id="modal-remove" uk-modal="true" className="uk-modal">
                                <div className="uk-modal-dialog uk-modal-body uk-margin-auto-vertical uk-border-rounded">
                                    <h4>Un-Available</h4>
                                    <p className="uk-text-large">Do you want to remove your device availability?</p>
                                    <p className="uk-text-right">
                                        <button className="uk-text-large uk-text-emphasis uk-margin-right uk-button uk-button-link uk-modal-close" type="button">Cancel</button>
                                        <button className="uk-text-large uk-text-emphasis uk-text-bold uk-button uk-button-link uk-modal-close" type="button">OK</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </form>
            </div>
           
        )
    }
}

export default Available;