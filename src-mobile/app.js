import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import axios from "axios";

import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";
import configureStore from "./configureStore";

import Menu from "../components/mobile/menu";
import QuickAccess from "../components/mobile/quickAccess";
import Footer from "../components/mobile/footer";

import Home from "./containers/home";
import Login from "./containers/login";
import Registration from "./containers/registration";
import DeviceDetail from "./containers/device";

import Cart from "./containers/cart";
import Compare from "./containers/compare";
import Payment from "./containers/payment";

import Account from "./containers/account";
import AccountDevice from "./containers/account/device";
import AccountDeviceAdd from "./containers/account/deviceAdd";
import AccountDeviceEdit from "./containers/account/deviceEdit";
import AccountDeviceImages from "./containers/account/deviceImages";
import AccountWishlist from "./containers/account/wishlist";
import AccountReceivedProposal from "./containers/account/receivedProposal";
import AccountReceivedProposalSale from "./containers/account/receivedProposalSale";
import AccountReceivedProposalExchange from "./containers/account/receivedProposalExchange";
import AccountInfo from "./containers/account/accountInfo";
import ChangePassword from "./containers/account/changePassword";
import Order from "./containers/account/order";
import Transaction from "./containers/account/transaction";

import AvailableAdd from "./containers/account/availableAdd";
import AvailableEdit from "./containers/account/availableEdit";

import Category from "./containers/category";
import Preview from "./containers/preview";
import Shipping from "./containers/shipping";

import Privacy from "./containers/link/privacy";
import Term from "./containers/link/term";
import SaleRetund from "./containers/link/saleRetund";
import Legal from "./containers/link/legal";
import SiteMap from "./containers/link/siteMap";
import HowItWork from "./containers/link/howItWork";
import AboutUs from "./containers/link/about";

import Search from "./containers/search";

import Functions from "../functions";
import {SERVER_SOCKET} from "../config";

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

import withStorage from "../hoc/storage";
import ScrollToTop from "../hoc/scrollToTop";

import io from 'socket.io-client';

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isMenu: true
        }
    }
    componentWillMount(){
        accounting.settings = {
            currency: {
                symbol : "$",   // default currency symbol is '$'
                format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 0   // decimal places
            },
            number: {
                precision : 0,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        }
        axios.interceptors.response.use(function (response) {
            const data = response.data;
            if(is.undefined(data)){
                UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
                return Promise.reject({status: 500});
            }
            else if(data.status !== 200)
                return Promise.reject(data);
            else
                return data.data;
        }, function(error) {
            UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
            return Promise.reject({status: 500});
        });

        const connectionOptions = {
            'force new connection': true,
            'reconnectionAttemps': 'Infinity',
            transports: ['websocket']
        }

        this.socket = io.connect(SERVER_SOCKET, connectionOptions);
        Functions.socketReceivedProposal(this.socket)
        .subscribe((observer) => {
            this.observer = observer;
            UIkit.notification({message: 'Your Proposal List Has Changed !', status: 'primary', pos: 'top-left'});
        })
    }
    render(){
        return (
            <Provider store={store}>
                <Router>
                    <ScrollToTop>
                        <div>
                            <div uk-height-viewport="true">
                                <div className="uk-container-extend">
                                    {this.state.isMenu && <Menu app={this}/>}
                                    <Route exact path="/" component={withStorage(Home)}/>
                                    <Route exact path="/login" render={
                                        (props) => (
                                            <Login {...props} app={this}/>
                                        )
                                    }/>
                                    <Route exact path="/register" component={Registration}/>
                                    <Route exact path="/device/:id" render={
                                        (props) => (
                                            <DeviceDetail key={props.match.params.id} {...props}/>
                                        )
                                    }/>
                                    <Route path="/account"
                                        render={
                                            props => (
                                                <Account {...props} socket={this.socket} routes={
                                                    [
                                                        {path: '/account/device', component: AccountDevice},
                                                        {path: '/account/wishlist', component: AccountWishlist},
                                                        {path: '/account/received_proposal', component: AccountReceivedProposal},
                                                        {path: '/account/information', component: AccountInfo},
                                                        {path: '/account/change_password', component: ChangePassword},
                                                        {path: '/account/order', component: Order},
                                                        {path: '/account/transactions', component: Transaction}
                                                    ]
                                                }/>
                                            )
                                        }
                                    />
                                    <Route exact path="/cart" render={
                                        (props) => (
                                            <Cart {...props} socket={this.socket}/>
                                        )
                                    }/>
                                    <Route exact path="/preview" component={Preview}/>
                                    <Route exact path="/shipping" component={Shipping}/>
                                    <Route exact path="/compare/:id" component={Compare}/>
                                    <Route exact path="/category" render={
                                        (props) => (
                                            <Category key={new Date().getUTCMilliseconds()} {...props}/>
                                        )
                                    }/>
                                    <Route exact path="/payment" render={
                                        (props) => (
                                            <Payment {...props} app={this}/>
                                        )
                                    }/>
                                    <Route exact path="/myaccount/device/add" component={AccountDeviceAdd}/>
                                    <Route exact path="/myaccount/device/edit/:id" component={AccountDeviceEdit}/>
                                    <Route exact path="/myaccount/device/images/:id" component={AccountDeviceImages}/>
                                    <Route exact path="/myaccount/device/:id/available/add" component={AvailableAdd}/>
                                    <Route exact path="/myaccount/device/:id/available/edit/:available_id" component={AvailableEdit}/>
                                    <Route exact path="/myaccount/received_proposal/sale/:id" render={
                                        (props) => (
                                            <AccountReceivedProposalSale {...props} socket={this.socket}/>
                                        )
                                    }/>
                                    <Route exact path="/myaccount/received_proposal/exchange/:id" render={
                                        (props) => (
                                            <AccountReceivedProposalExchange {...props} socket={this.socket}/>
                                        )
                                    }/>
                                    <Route exact path="/privacy" component={Privacy}/>
                                    <Route exact path="/term-of-use" component={Term}/>
                                    <Route exact path="/sale-and-retunds" component={SaleRetund}/>
                                    <Route exact path="/legal" component={Legal}/>
                                    <Route exact path="/sitemap" component={SiteMap}/>
                                    <Route exact path="/how-it-work" component={HowItWork}/>
                                    <Route exact path="/about-us" component={AboutUs}/>
                                    
                                    <Route exact path="/search/:search" render={
                                        (props) => (
                                            <Search key={props.match.params.search} {...props}/>
                                        )
                                    }/>

                                    {this.state.isMenu && <QuickAccess/>}
                                    <Footer/>
                                    
                                </div>
                            </div>
                           
                        </div>
                    </ScrollToTop>
                </Router>
            </Provider>
        )
    }
}

export default App;