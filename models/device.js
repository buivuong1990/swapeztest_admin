import axios from "axios";
import { SERVER_API } from "../config";

export default {
    getListMyDevice:({email, offset, limit})=>{
        return axios.post(SERVER_API+'device/list',{email, offset, limit});
    },
    recommendDeviceEmpty:({id, recommend_device})=>{
        return axios.post(SERVER_API+'proposal/recommend/delete',{id, recommend_device});
    },
    recommendDevice:({id, recommend_device})=>{
        return axios.post(SERVER_API+'proposal/recommend/add',{id, recommend_device});
    },
    searchWishList: ({email,id}) => {
        return axios.post(SERVER_API+'device/searchWishList', {email,id});
    },
    addSearchDevice: ({category_id, brand_id, model_id, capacity_id, color_id, ram_id, email}) => {
        return axios.post(SERVER_API+'device/search/add', {category_id, brand_id, model_id, capacity_id, color_id, ram_id, email});
    },
    deleteSearchItem: ({id}) => {
        return axios.delete(SERVER_API+'device/search/delete',  { data: { id: id } });
    },
    getSearchList: ({email}) => {
        return axios.post(SERVER_API+'device/list/search', {email: email});
    },
    checkExitsSearch: ({model_id,  capacity_id, color_id, ram_id, email}) => {
        return axios.post(SERVER_API+'device/search/exists', {model_id, capacity_id, color_id, ram_id, email});
    },
    getSearchByName:({device_name, email}) => {
        return axios.post(SERVER_API+'device/searchByDeviceName',{device_name, email});
    },
    getListImage:({device_id}) => {
        return axios.post(SERVER_API+'device/list/image',{device_id});
    },
    getDeviceWithType: ({email, type, offset, limit}) => {
        return axios.post(SERVER_API+'device/list/type', {email, type, offset, limit});
    },
    getDeviceWithTypeMobile: ({email, type}) => {
        return axios.post(SERVER_API+'deviceMobile/list/type', {email: email, type: type});
    },
    getDeviceFeatured: ({email}) => {
        return axios.post(SERVER_API+'device/list/featured', {email: email});
    },
    getDeviceDetail: ({id, email}) => {
        return axios.get(SERVER_API+'device/detail/'+id+'/'+email);
    },
    getDeviceRelated: ({id, email}) => {
        return axios.get(SERVER_API+'device/related/'+id+'/'+email);
    },
    getDeviceWishlist: ({email}) => {
        return axios.post(SERVER_API+'device/list/wishlist', {email: email});
    },
    getDeviceListByUser: ({email}) => {
        return axios.post(SERVER_API+'device/list', {email: email});
    },
    getBrandList: () => {
        return axios.get(SERVER_API+'brand/list');
    },
    getCategoryList: (search) => {
        return axios.post(SERVER_API+'category/list', {search: search});
    },
    getColorList: () => {
        return axios.get(SERVER_API+'color/list');
    },
    getCapacityList: () => {
        return axios.get(SERVER_API+'capacity/list');
    },
    getRamList: () => {
        return axios.get(SERVER_API+'ram/list');
    },
    getDeviceCategory: ({email, filter, offset,limit}) => {
        console.log("dsad");
        return axios.post(SERVER_API+'device/category', {email, filter, offset,limit});
    },
    getDeviceCompare: ({id, email}) => {
        return axios.post(SERVER_API+'device/compare', {email, id});
    },

    // Admin
    insertCategory: ({name}) => {
        return axios.post(SERVER_API+'category/add', {name});
    },
    editCategory:({id, name})=>{
        return axios.post(SERVER_API+'category/edit', {id, name});
    },
    deleteCategory:({id}) => {
        return axios.delete(SERVER_API+'category/delete',  { data: { id: id } });
    },
    getCategoryDetail:({id})=>{
        return axios.get(SERVER_API+'category/detail/'+id);
    },
    getBrandList: () => {
        return axios.get(SERVER_API+'brand/list');
    },
    insertBrand: ({name}) => {
        return axios.post(SERVER_API+'brand/add', {name});
    },
    editBrand:({id, name})=>{
        return axios.post(SERVER_API+'brand/edit', {id, name});
    },
    getBrandDetail:({id})=>{
        return axios.get(SERVER_API+'brand/detail/'+id);
    },
    deleteBrand:({id}) => {
        return axios.delete(SERVER_API+'brand/delete',  { data: { id: id } });
    },
    getBrandImage:({id})=>{
        return axios.get(SERVER_API+'brand/image/'+id);
    },
    deleteBrandImage:({id}) => {
        return axios.delete(SERVER_API+'brand/delete/image',  { data: { id: id } });
    },
    getModelList: () => {
        return axios.get(SERVER_API+'model/list');
    },
    insertModel: ({name, category_id, brand_id, color_id, capacity_id, ram_id, price}) => {
        return axios.post(SERVER_API+'model/add', {name, category_id, brand_id, color_id, capacity_id, ram_id, price});
    },
    deleteModel:({id}) => {
        return axios.delete(SERVER_API+'model/delete',  { data: { id: id } });
    },
    editModel:({id, name, category_id, brand_id, color_id, capacity_id, ram_id, price})=>{
        return axios.post(SERVER_API+'model/edit', {id, name, category_id, brand_id, color_id, capacity_id, ram_id, price});
    },
    getModelDetail:({id})=>{
        return axios.get(SERVER_API+'model/detail/'+id);
    },
    getModelImage:({id})=>{
        return axios.get(SERVER_API+'model/image/'+id);
    },
    deleteModelImage:({id}) => {
        return axios.delete(SERVER_API+'model/delete/image',  { data: { id: id } });
    },
    getColorList: () => {
        return axios.get(SERVER_API+'color/list');
    },
    insertColor: ({name, code}) => {
        return axios.post(SERVER_API+'color/add', {name, code});
    },
    editColor:({id, name, code})=>{
        return axios.post(SERVER_API+'color/edit', {id, name, code});
    },
    getColorDetail:({id})=>{
        return axios.get(SERVER_API+'color/detail/'+id);
    },
    deleteColor:({id}) => {
        return axios.delete(SERVER_API+'color/delete',  { data: { id: id } });
    },
    getCapacity: () => {
        return axios.get(SERVER_API+'capacity/list');
    },
    insertCapacity: ({name}) => {
        return axios.post(SERVER_API+'capacity/add', {name});
    },
    deleteCapacity:({id}) => {
        return axios.delete(SERVER_API+'capacity/delete',  { data: { id: id } });
    },
    getCapacityDetail:({id})=>{
        return axios.get(SERVER_API+'capacity/detail/'+id);
    },
    editCapacity:({id, name})=>{
        return axios.post(SERVER_API+'capacity/edit', {id, name});
    },
    getRamList: () => {
        return axios.get(SERVER_API+'ram/list');
    },
    insertRam: ({name}) => {
        return axios.post(SERVER_API+'ram/add', {name});
    },
    deleteRam:({id}) => {
        return axios.delete(SERVER_API+'ram/delete',  { data: { id: id } });
    },
    getRamDetail:({id})=>{
        return axios.get(SERVER_API+'ram/detail/'+id);
    },
    editRam:({id, name})=>{
        return axios.post(SERVER_API+'ram/edit', {id, name});
    },
    getImeiList: () => {
        return axios.get(SERVER_API+'imei/list');
    },
    deleteImei:({id}) => {
        return axios.delete(SERVER_API+'imei/delete',  { data: { id: id } });
    },
    insertImei: ({imei, model_id}) => {
        return axios.post(SERVER_API+'imei/add', {imei, model_id});
    },
    getImeiDetail:({id})=>{
        return axios.get(SERVER_API+'imei/detail/'+id);
    },
    editImei:({id, imei, model_id})=>{
        return axios.post(SERVER_API+'imei/edit', {id, imei, model_id});
    },
    getSlideList: () => {
        return axios.get(SERVER_API+'slide/list');
    },
    insertSlide: ({name}) => {
        return axios.post(SERVER_API+'slide/add', {name});
    },
    deleteSlide:({id}) => {
        return axios.delete(SERVER_API+'slide/delete',  { data: { id: id } });
    },
    editSlide:({id, name})=>{
        return axios.post(SERVER_API+'slide/edit', {id, name});
    },
    getSlideDetail:({id})=>{
        return axios.get(SERVER_API+'slide/detail/'+id);
    },
    getSlideImage:({id})=>{
        return axios.get(SERVER_API+'slide/image/'+id);
    },
    deleteSlideImage:({id}) => {
        return axios.delete(SERVER_API+'slide/delete/image',  { data: { id: id } });
    },
}