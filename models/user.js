import axios from "axios";
import { SERVER_API , MICRO_DOMAIN, MICRO_AUTH} from "../config";

export default {
    getTransactionList: ({ email }) => {
        return axios.post(SERVER_API + 'user/transaction/list', { email });
    },
    getOrderList: ({ email }) => {
        return axios.post(SERVER_API + 'user/order/list', { email });
    },
    checkPassword: ({ email, password }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/checkPassword', { email, password });
    },
    changePassword: ({ email, password }) => {
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/changePassword', { email, password });
    },
    checkExists: ({ email }) => {
        return axios.post(SERVER_API + 'user/exists', { email });
    },
    registerUser: ({ email, password }) => {
        return axios.post(SERVER_API + 'user/register', { email, password });
    },
    addOrder: ({ email, list, total }) => {
        return axios.post(SERVER_API + 'user/order', { email, list, total });
    },
    editUser: ({ email, full_name, city, country, address1, address2, postal_code }) => {
        return axios.post(SERVER_API + 'user/edit', { email, full_name, city, country, address1, address2, postal_code });
    },
    getUserDetail: ({ email }) => {
        return axios.post(SERVER_API + 'user/detail', { email: email });
    },
    deleteItemCart: ({ id }) => {
        return axios.delete(SERVER_API + 'user/cart', { data: { id: id } });
    },
    postItemToCart: ({ email, device_id, available_id, type }) => {
        return axios.post(SERVER_API + 'user/cart', { email: email, device_id: device_id, available_id: available_id, type });
    },
    getListCartByUser: ({ email }) => {
        return axios.post(SERVER_API + 'user/cart/email', { email: email });
    },
    getItemCart: ({ id }) => {
        return axios.get(SERVER_API + 'user/cart/' + id);
    },
    addWishlist: ({ email, device_id }) => {
        return axios.post(SERVER_API + 'user/wishlist', { email: email, device_id: device_id });
    },
    removeWishlist: ({ id }) => {
        return axios.delete(SERVER_API + 'user/wishlist', { data: { id: id } });
    },
    addProposal: ({ email, cart_id, type, sale_proposal_price, exchange_proposal_price, exchange_proposal_device }) => {
        return axios.post(SERVER_API + 'proposal/add', { email, cart_id, type, sale_proposal_price, exchange_proposal_device, exchange_proposal_price, status });
    },
    editProposal: ({ email, cart_id, type, sale_proposal_price, exchange_proposal_price, exchange_proposal_device, id, status = "updated" }) => {
        return axios.post(SERVER_API + 'proposal/edit', { id, email, cart_id: cart_id, type, sale_proposal_price, exchange_proposal_device, exchange_proposal_price, status });
    },
    cancelProposal: ({ id }) => {
        return axios.delete(SERVER_API + 'proposal/cancel', { data: { id: id } });
    },
    getListReceivedProposal: ({ email }) => {
        return axios.post(SERVER_API + 'proposal/received/list', { email: email });
    },
    getDetailReceivedProposal: ({ email, id }) => {
        return axios.get(SERVER_API + 'proposal/received/detail/' + id + '/' + email);
    },
    loginAnonymous: ({ ip }) => {
        return axios.post(SERVER_API + 'user/login/anonymous', { ip });
    },
    syncCart: ({ prev_email, email }) => {
        return axios.post(SERVER_API + 'user/cart/sync', { prev_email, email });
    }
  
}