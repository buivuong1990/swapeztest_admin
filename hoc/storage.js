import React from "react";
import UserModel from "../models/user";
import {SERVER_ANONYMOUS} from "../config";

const withStorage = (WrappedComponent) => {
    class HOC extends React.Component{
        constructor(props){
            super(props);
            this.state = {
                auth: '',
                anonymous: false
            }
        }
        componentDidMount(){
            const admin = localStorage.getItem('admin') ? localStorage.getItem('admin') : null;
            const token_admin = localStorage.getItem('token_admin') ? localStorage.getItem('token_admin'): null;
            const auth = admin ? admin : null;
            if(!auth){
                this.props.history.push('/login');
            }else{
                localStorage.setItem('admin', admin);
                localStorage.setItem('token_admin', token_admin);
            }
        }
        render(){
            const props = {
                ...this.props,
                auth: this.state.auth,
                anonymous: this.state.anonymous
            }
            if(this.state.auth)
                return <WrappedComponent {...props} ref={instance => this.wrappedComponentRef = instance}/>
            else
                return null
        }
    }
    return HOC;
}

export default withStorage;