var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();
var device = require('express-device');

app.set('views', path.resolve(__dirname, 'dist'));
app.set('views', path.resolve(__dirname, 'dist-mobile'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(device.capture());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname+'/plugins'));
app.use(express.static(__dirname+'/dist'));
app.use(express.static(__dirname+'/dist-mobile'));
app.use(express.static(__dirname+'/images'));

app.get('/*', function (req, res) {
  if (req.device.type === 'phone') {
		res.sendFile(path.join(__dirname + '/dist-mobile/mobile.html'));
	}
	else {
		res.sendFile(path.join(__dirname+ '/dist/desktop.html'));
	}
});

app.listen(9090, function () {
    console.log('Example listening on port 9090!');
});

module.exports = app;