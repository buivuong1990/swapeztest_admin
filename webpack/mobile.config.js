const HtmlWebPackPlugin = require("html-webpack-plugin");
const path = require('path');
const webpack = require('webpack');

module.exports = env => ({
    mode: 'development',
    entry: path.join(__dirname, '..', 'src-mobile', 'index.js'),
    output: {
        path: path.join(__dirname, '..', 'server', 'dist-mobile'),
        filename: 'mobile.bundle.js',
        publicPath: '/'
    },
    module: {
      rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: "babel-loader"
            }
        },
        {
            test: /\.html$/,
            use: [
                {
                    loader: "html-loader",
                    options: { minimize: true }
                }
            ]
        }
      ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: "./src-mobile/index.html",
            filename: "./mobile.html"
        }),
        new webpack.DefinePlugin({
            WEBPACK_MODE: JSON.stringify(env)
        }),
    ]
});