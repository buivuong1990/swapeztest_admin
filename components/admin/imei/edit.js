import React from "react";
import { Select2, Cover, Spinner, Button} from "../../../components/common";
import { Form, Field } from 'react-final-form';
import DeviceModel from "../../../models/device";
import { withRouter } from "react-router";

class ImeiEdit extends React.Component {
    constructor(props){
        super(props);
        this.id = 0;
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            modelList: [],
            name: '',
            model:{},
        }
    }
    componentWillMount(){   
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        this.handlerApiModel();
        DeviceModel.getImeiDetail({id:this.id})
        .then(detail=>{
            this.setState({imei: detail.imei, model: {id: detail.model_id, model_name: detail.model_name}});
        })
    }
    handlerApiModel(){
        DeviceModel.getModelList()
        .then(result => {
            this.setState({modelList:result});
        })
    }
    handlerSubmit(values){
         return new Promise((resolve, reject) => {
            const model_id = values.model.id;
           
            const imei = values.imei;
            DeviceModel.editImei({id:this.id,imei, model_id})
            .then(result =>{
                UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
                this.props.history.push('/imei/list');
                 resolve();
            })

         })
    }
    render(){
        return(
            <div className="uk-padding-small">
                 <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/imei/list')}>Imei</a></li>
                    <li><span>Edit</span></li>
                </ul>
                <Form
                    initialValues={{
                        model: this.state.model,

                        imei: this.state.imei
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                        const errors = {};
                            if(!values.imei)
                                errors.imei = "Imei must be choose";
                            if(!values.model)
                                errors.model = "Model must be choose";
                           
                        return errors;
                    }}
                    render={({ submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        this.values = values;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                <div className="uk-grid" uk-grid="true">
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="imei">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <div className="">
                                                    <label className="uk-form-label uk-text-bold">Imei</label>
                                                    <div className="uk-form-controls uk-margin-xsmall-top">
                                                        <input {...input} className="uk-input" type="text" placeholder="Imei..." />
                                                    </div>
                                                </div>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="model">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Model" id="model"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="model_name"
                                                        optionValue="id"
                                                        options={this.state.modelList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    
                                
                          
                                    <div className="uk-width-1-1 uk-margin">    
                                        <Button className="uk-width-auto uk-modal-close" type="submit" disabled={submitting || validating} color="primary">
                                            Choose
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }}/>
            </div>
        )
    }
}
export default withRouter(ImeiEdit);