import React from "react";
import { Form, Field } from 'react-final-form';
import { Cover, Spinner, Button, Select2 } from "../../../components/common";
import DeviceModel from "../../../models/device";

class ImeiAdd extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            modelList:[]
        }
        this.handlerSubmit = this.handlerSubmit.bind(this);
    }
    componentDidMount(){
        this.handlerApiModel();
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.setState({isLoading:true});
            console.log("das");
            const model_id = values.model.id;
            
            DeviceModel.insertImei({imei: values.imei, model_id:model_id})
            .then((result) => {
                this.props.history.push('/imei/list');
            })
            .catch(() => {
                resolve();
            })
        })
    }
    handlerApiModel(){
        DeviceModel.getModelList()
        .then(result => {
            this.setState({modelList:result});
        })
    }
    render(){
        return(
            <div className="uk-padding-small">
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/imei/list')}>Imei</a></li>
                    <li><span>Add</span></li>
                </ul>
                <div>
                    <Form
                        onSubmit={this.handlerSubmit}
                        validate={values => {
                                const errors = {};
                                if(!values.imei)
                                    errors.imei = "Imei name must be required";
                                if(!values.model)
                                    errors.model = "Model name must be required";
                                
                                return errors;
                            }
                        }
                        render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit}>
                                <div className="uk-inline uk-width-1-1">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    <Field name="imei">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column">
                                            <div className="">
                                                <label className="uk-form-label uk-text-bold">Imei</label>
                                                <div className="uk-form-controls uk-margin-xsmall-top">
                                                    <input {...input} className="uk-input uk-width-1-2" type="text" placeholder="Imei..." />
                                                </div>
                                            </div>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>       
                                    <div className="uk-margin">
                                        <Field name="model">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="model">Model</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Model" id="model"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="model_name"
                                                        optionValue="id"
                                                        options={this.state.modelList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>                    
                                    <div className="uk-flex uk-margin">
                                        <Button className="uk-button" type="submit" disabled={submitting || validating} color="primary">
                                            Save
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}>
                    </Form>
                </div>
            </div>
        )
    }
}
export default ImeiAdd;