import React from "react";
import ReactTable from 'react-table';
import DeviceModel from "../../../models/device";
import Confirm from "../confirm";
import { SERVER_IMG, SERVER_UPLOAD } from "../../../config";


class ModelList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            list : [],
            showConfirm: false
        }
        this.selectedId = 0;
    }
    componentDidMount(){
        this.listModel();
    }
    componentWillUnmount(){
        this.selectedId = 0;
    }
    listModel(){
        DeviceModel.getModelList()
        .then(result =>{
            this.setState({list: result});
        })
    }
    onOpenConfirm(row){
        this.setState({ showConfirm: true }, () => {
            this.selectedId = row.value;
        });
    }
    onClose(){
        this.setState({ showConfirm: false }, () => {
            this.selectedId = 0;
        });
    }
    onDelete(){
        DeviceModel.deleteModel({id: this.selectedId})
        .then(()=>{
            this.setState({showConfirm:false}, ()=>{
                this.listModel();
                this.selectedId = 0;
            })
        })
    }
    render(){
        const data = this.state.list;

        const columns = [
            {
                Header: 'No.',
                id: 'row',
                maxWidth: 100,
                filterable: false,
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.index+1}</div>
                }
            },
            {
                Header: 'Name',
                accessor: 'model_name',
                Cell: (row) => {
                    return <div style={{textAlign:'left'}}>{row.value}</div>
                }
            },
            {
                Header: 'Category',
                accessor: 'category_name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header: 'Brand',
                accessor: 'brand_name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header: 'Color',
                accessor: 'color_name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header: 'Capacity',
                accessor: 'capacity_name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header: 'Ram',
                accessor: 'ram_name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header: 'Price',
                accessor: 'price',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}$</div>
                }
            },
            {
                Header: 'Image ',
                accessor: 'url',
                Cell: (row) => {
                    if (row.value)
                        return <div style={{textAlign:'center'}}><img src={SERVER_IMG+'/models/'+row.value} width="30px"/></div>
                    else
                        return <div style={{textAlign:'center'}}><img src={SERVER_UPLOAD+'no-image.png'} width="30px"/></div>
                }
            },
            {
                Header:'',
                accessor: 'id',
                Cell: (row) => {
                    return <div>
                        <ul className="uk-iconnav">
                        <li><a onClick={() => this.props.history.push('/model/image/'+row.value)} uk-icon="icon: image"></a></li>
                            <li><a onClick={() => this.props.history.push('/model/edit/'+row.value)} uk-icon="icon: file-edit" ></a></li>
                            <li><a onClick={this.onOpenConfirm.bind(this, row)} uk-icon="icon: trash"></a></li>
                        </ul>
                    </div>
                }
            }
        ];

        return (
            <div className="uk-padding-small">
                <Confirm show={this.state.showConfirm}
                    title="Are you sure Delete?"
                    onCancel={this.onClose.bind(this)}
                    onOK={this.onDelete.bind(this)}/>
                <div className="uk-flex uk-flex-between">
                    <ul className="uk-breadcrumb">
                        <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                        <li><span>Model</span></li>
                    </ul>
                    <div>
                        <a onClick={() => this.props.history.push('/model/add')} className="uk-link-reset"><span uk-icon="icon: plus-circle"></span> ADD NEW</a>
                    </div>
                </div>
                <ReactTable
                    data={data}
                    columns={columns}       
                    defaultPageSize={10}/>
            </div>
        )
    }
}
export default ModelList;