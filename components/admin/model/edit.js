import React from "react";
import { Select2, Cover, Spinner, Button} from "../../../components/common";
import { Form, Field } from 'react-final-form';
import DeviceModel from "../../../models/device";
import CurrencyInput from 'react-currency-input';
import { withRouter } from "react-router";

class ModelEdit extends React.Component {
    constructor(props){
        super(props);
        this.id = 0;
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            categoryList: [],
            brandList: [],
            colorList: [],
            capacityList:[],
            ramList:[],
            model: '',
            price: '',
            category: {},
            brand: {},
            color:{},
            capacity:{},
            ram:{},
        }
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        this.handlerApiCategory();
        this.handlerApiBrand();
        this.handlerApiColor();
        this.handlerApiCapacity();
        this.handlerApiRam();
        DeviceModel.getModelDetail({id:this.id})
        .then(detail=>{
            this.setState({model:detail.model_name, price: detail.price,
                category: {id: detail.category_id, name: detail.category_name},
                brand: {id: detail.brand_id, name: detail.brand_name},
                color: {id:detail.color_id, name:detail.color_name},
                capacity: {id:detail.capacity_id, name:detail.capacity_name},
                ram: {id:detail.ram_id, name:detail.ram_name},
            });
        })
    }
    handlerApiCategory(){
        DeviceModel.getCategoryList({name: ''})
        .then(result => {
            this.setState({categoryList: result});
        })
    }
    handlerApiBrand(){
        DeviceModel.getBrandList()
        .then(result => {
            this.setState({brandList:result});
        })
    }
    handlerApiColor(){
        DeviceModel.getColorList()
        .then(result => {
            this.setState({colorList:result});
        })
    }
    handlerApiCapacity(){
        DeviceModel.getCapacityList()
        .then(result => {
            this.setState({capacityList:result});
        })
    }
    handlerApiRam(){
        DeviceModel.getRamList()
        .then(result => {
            this.setState({ramList:result});
        })
    }
    handlerSubmit(values){
         return new Promise((resolve, reject) => {
            const category_id = values.category.id;
            const brand_id = values.brand.id;
            const color_id = values.color.id;
            const capacity_id = values.capacity.id;
            const ram_id = values.ram.id;
            const name = values.model;
            const price = values.price;
            
            DeviceModel.editModel({id:this.id,name, category_id, brand_id, color_id, capacity_id, ram_id, price})
            .then(result =>{
                
                UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
                this.props.history.push('/model/list');
                 resolve();
            })

         })
    }
    render(){
        return(
            <div className="uk-padding-small">
                 <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/model/list')}>Model</a></li>
                    <li><span>Edit</span></li>
                </ul>
                <Form
                    initialValues={{
                        model: this.state.model,
                        category: this.state.category,
                        brand: this.state.brand,
                        color: this.state.color,
                        capacity: this.state.capacity,
                        ram: this.state.ram,
                        price: this.state.price,
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                        const errors = {};
                            if(!values.category)
                                errors.category = "Category must be choose";
                            if(!values.brand)
                                errors.brand = "Brand must be choose";
                            if(!values.color)
                                errors.color = "Color must be choose";
                            if(!values.capacity)
                                errors.capacity = "Capacity must be choose";
                            if(!values.ram)
                                errors.ram = "Ram must be choose";
                            if(!values.model)
                                errors.model = "Model must be choose";
                            if(!values.price)
                                errors.price = "Price must be choose";
                           
                        return errors;
                    }}
                    render={({ submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.form = form;
                        this.values = values;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                <div className="uk-grid" uk-grid="true">
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="model">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <div className="">
                                                    <label className="uk-form-label uk-text-bold">Model</label>
                                                    <div className="uk-form-controls uk-margin-xsmall-top">
                                                        <input {...input} className="uk-input" type="text" placeholder="Model..." />
                                                    </div>
                                                </div>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="category">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="category">Category</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Category" id="category"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="name"
                                                        optionValue="id"
                                                        options={this.state.categoryList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="brand">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="brand">Brand</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Brand" id="brand"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="name"
                                                        optionValue="id"
                                                        options={this.state.brandList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="color">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="color">Color</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Color" id="color"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="name"
                                                        optionValue="id"
                                                        options={this.state.colorList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="capacity">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="capacity">Capacity</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Capacity" id="capacity"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="name"
                                                        optionValue="id"
                                                        options={this.state.capacityList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="ram">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold" htmlFor="ram">Ram</label>
                                                <div className="uk-inline">
                                                    <span className="uk-form-icon" uk-icon="icon: check"/>
                                                    <Select2 {...input} 
                                                        placeholder="Choose Ram" id="ram"
                                                        value={input.value}
                                                        onChange={value => input.onChange(value)}
                                                        optionLabel="name"
                                                        optionValue="id"
                                                        options={this.state.ramList}
                                                    />
                                                </div>
                                                {
                                                    meta.touched && meta.error &&
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error || meta.submitError}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    <div className="uk-width-1-2@l uk-width-1-1@m uk-margin uk-margin-remove-top">
                                        <Field name="price">
                                            {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                                <div className="">
                                                    <label className="uk-form-label uk-text-bold">Price</label>
                                                    <div className="uk-form-controls uk-margin-xsmall-top">
                                                        <CurrencyInput 
                                                            precision="0"
                                                            prefix="$"
                                                            className="uk-input"
                                                            value={input.value} onChangeEvent={(event, maskedValue, floatValue) => {
                                                            input.onChange(floatValue);
                                                        }}/>
                                                    </div>
                                                </div>
                                                {
                                                    meta.error && meta.touched && 
                                                    <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                        {meta.error}
                                                    </span>
                                                }
                                            </div>
                                            )}
                                        </Field>
                                    </div>
                                    
                                    <div className="uk-width-1-1 uk-margin">    
                                        <Button className="uk-width-auto uk-modal-close" type="submit" disabled={submitting || validating} color="primary">
                                            Choose
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    }}/>
            </div>
        )
    }
}
export default withRouter(ModelEdit);