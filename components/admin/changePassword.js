import React from "react";

import { Form, Field } from 'react-final-form';
import { InputPassword, Cover, Spinner, Button } from "../common";


import UserModel from "../../models/user";
import {withRouter} from "react-router-dom";

class ChangePassword extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isRedirect: false,
            isLoadingExists: false
        }
        this.resetForm = null;
    }
    componentWillUnmount(){
        this.resetForm = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            const email = localStorage.getItem('admin') ? localStorage.getItem('admin'): null;
            UserModel.checkPassword({email: email, password: values.password})
            .then((result)=>{
                UserModel.changePassword({email: email, password: values.new_password})
                .then(() => {
                    UIkit.notification({message: 'Change Password Successfully.', status: 'primary', pos: 'top-left'});
                    this.props.history.push('/category/list');
                })
                .catch(() => {
                    resolve();
                });
            })
            .catch(()=>{
                UIkit.notification({message: 'Old Password Is Not Correct !', status: 'danger', pos: 'top-left'});
                resolve();
            })
        })
    }
    render(){
        return (
            <div className="uk-padding-large">
                <h3 className="uk-margin-remove-top">Change Password</h3>
                <div className="uk-margin">
                    <Form
                        onSubmit={this.handlerSubmit}
                        validate={values => {
                                const errors = {};

                                if(!values.password)
                                    errors.password = "Password must be required";
                                else if(values.password.length < 6)
                                    errors.password = "Password must > 6 characters";
                                else if(values.password.length > 50)
                                    errors.password = "Password must < 50 characters";

                                if(!values.new_password)
                                    errors.new_password = "New Password must be required";
                                else if(values.new_password.length < 6)
                                    errors.new_password = "New Password must > 6 characters";
                                else if(values.new_password.length > 50)
                                    errors.new_password = "New Password must < 50 characters";

                                else if(values.confirm_password !== values.new_password)
                                    errors.confirm_password = "Confirm Password does not match";
                                
                                return errors;
                            }
                        }
                        render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit} >
                                <div className="uk-inline uk-width-1-3@l uk-width-1-2@m uk-width-1-1@s">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    <Field name="password">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column uk-margin">
                                         <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold">Old Password</label>
                                            <InputPassword {...input} placeholder="Old Password"/>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
                                    <Field name="new_password">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column uk-margin">
                                         <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold">New Password</label>
                                            <InputPassword {...input} placeholder="New Password"/>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
                                    <Field name="confirm_password">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column uk-margin">
                                         <label className="uk-form-label uk-margin-xsmall-bottom uk-text-bold">Confirm New Password</label>
                                            <InputPassword {...input} placeholder="Confirm New Password"/>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
    
                                    <div className="uk-flex uk-margin-large">
                                        <Button className="uk-width-1-1" type="submit" disabled={submitting || validating} color="primary">
                                            Change Password
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}
                    >
                    </Form>
                </div>
            </div>
        )
    }
}



export default ChangePassword;