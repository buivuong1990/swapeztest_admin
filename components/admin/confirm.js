import React from "react";

import {Modal} from "react-overlays";

class Confirm extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const backdropStyle = {
            position: 'fixed',
            zIndex: 1040,
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: '#000',
            opacity: 0.5
        };
        const modalStyle = function() {
            return {
              position: 'fixed',
              width: 400,
              zIndex: 1040,
              top: '50%',
              left: '50%',
              transform: 'translate(-50%,-50%)',
              border: '1px solid #e5e5e5',
              backgroundColor: 'white',
              boxShadow: '0 5px 15px rgba(0,0,0,.5)',
              padding: 20
            };
        };
        return (
            <Modal
                onHide={this.props.onCancel}
                style={modalStyle()}
                show={this.props.show}
                renderBackdrop={(props) => {
                    return <div {...props} style={backdropStyle} />;
                }}>
                <div>
                    <h4>{this.props.title}</h4>
                    <div>
                        <button className="uk-button uk-button-primary uk-margin-right"
                            onClick={this.props.onOK}>OK</button>
                        <button onClick={this.props.onCancel} className="uk-button uk-button-default">Cancel</button>
                    </div>
                </div>
            </Modal>
        )
    }
}

export default Confirm;