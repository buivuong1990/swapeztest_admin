import React from "react";
import DeviceModel from "../../../models/device";
import { Form, Field } from 'react-final-form';
import { Cover, Spinner, Button } from "../../../components/common";
import { withRouter } from "react-router";

class SlideEdit extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isLoading:false,
            name: ''
        }
        this.id = 0;
        this.handlerSubmit = this.handlerSubmit.bind(this);
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        DeviceModel.getSlideDetail({id: this.id})
        .then(detail =>{
           const {name} = detail;
           this.setState({name});
        })
    }
    handlerSubmit(values){
        this.setState({isLoading: true});
        DeviceModel.editSlide({id:this.id, name:values.name})
        .then(()=>{
            this.setState({isLoading: false});
            UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
            this.props.history.push('/slide/list');
        })
    }
    render(){
        return (
            <div className="uk-padding-small">
                 <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/slide/list')}>Slide</a></li>
                    <li><span>Edit</span></li>
                </ul>
                <Form
                    initialValues={{
                        name: this.state.name
                    }}
                    onSubmit={this.handlerSubmit}
                    validate={values => {
                            const errors = {};
                            if(!values.name)
                                errors.name = "Slide name must be required";
                            
                            return errors;
                        }
                    }
                    render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                        this.resetForm = form.reset;
                        return <form onSubmit={handleSubmit}>
                            <div className="uk-inline uk-width-1-1">
                                {submitting && <Cover/>}
                                {submitting && <Spinner/>}
                                <Field name="name">
                                    {({ input, meta }) => (
                                    <div className="uk-flex uk-flex-column">
                                        <div className="">
                                            <label className="uk-form-label uk-text-bold">Slide</label>
                                            <div className="uk-form-controls uk-margin-xsmall-top">
                                                <input {...input} className="uk-input uk-width-1-2" type="text" placeholder="Slide..." />
                                            </div>
                                        </div>
                                        {
                                            meta.error && meta.touched && 
                                            <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                {meta.error}
                                            </span>
                                        }
                                    </div>
                                    )}
                                </Field>                           
                                <div className="uk-flex uk-margin">
                                    <Button className="uk-button" type="submit" disabled={submitting || validating} color="primary">
                                        Save
                                    </Button>
                                </div>
                            </div>
                        </form>
                    }}>
                </Form>
            </div>
        )
    }
}
export default withRouter(SlideEdit);