import React from "react";

class Imei extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <div className="uk-padding-small">
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}>Dashboard</a></li>
                    <li className="uk-disabled"><a>Imei</a></li>
                </ul>
                <div>
                    <form>
                        <div className="uk-margin">
                            <label className="uk-form-label uk-text-bold">Imei</label>
                            <div className="uk-form-controls uk-margin-xsmall-top">
                                <input className="uk-input uk-width-1-2" type="text" placeholder="Input..." />
                            </div>
                        </div>
                        <button className="uk-button uk-button-primary">Save</button>
                    </form>
                </div>
            </div>
               
    
        )
    }
}
export default Imei;























