import React from "react";
import axios from "axios";
import { MICRO_AUTH, MICRO_DOMAIN, MICRO_EVENT } from "../../../config";

import { SERVER_API, SERVER_IMG  } from "../../../config";
import {Redirect} from "react-router";

import { Form, Field } from 'react-final-form';
import { InputText, InputPassword, Cover, Spinner, Button, Alert } from "../../common";

class Login extends React.Component{
    constructor(props){
        super(props);
        this.handlerSubmit = this.handlerSubmit.bind(this);
        this.state = {
            isRedirect: false,
            isServerError: false
        }
        this.resetForm = null;
    }
    componentDidMount(){
        $('#sidemenu').hide();
    }
    componentWillUnmount(){
        $('#sidemenu').show();
        this.resetForm = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.handlerApiLogin(values)
            .then(result => {
                values.token = result.token;
                this.handlerGlobalStorage(values);
            })
            .catch(error => {
                if(error.status === 400)
                    this.setState({isServerError: true});
                this.resetForm();
                resolve();
            })
        })
    }
    handlerApiLogin(values){
        return axios.post(MICRO_DOMAIN+MICRO_AUTH + '/api/user/login/admin', {email: values.email, password: values.password});
    }
    handlerGlobalStorage(values){
        UIkit.notification({message: 'Login Successfully !', status: 'primary', pos: 'top-left'});
        localStorage.setItem('admin', values.email);
        localStorage.setItem('token_admin', values.token);
        axios.defaults.headers.common['Authorization'] = 'JWT '+ values.token.toString();
        console.log('category/list', this.props.history);
        setTimeout(() => {
            this.props.history.push('/category/list');
        }, 1000);
    }
    render(){
        return (
            <div className="uk-flex uk-flex uk-flex-center uk-flex-middle uk-background-cover" uk-height-viewport="true">
                {this.state.isRedirect ? <Redirect to="/"/> : null}
                <div className="uk-background-default uk-padding-large uk-width-1-3@l uk-width-1-2@m uk-width-2-3@s">
                    <div className="uk-grid uk-grid-divider uk-child-width-expand@s uk-flex uk-flex-middle" uk-grid="true">
                        <div>
                        <div className="uk-flex uk-flex-center uk-text-bold uk-h3">LOGIN ACCOUNT</div>
                            <Form
                                onSubmit={this.handlerSubmit}
                                validate={values => {
                                        const errors = {};
                                        if(!values.email)
                                            errors.email = "Email must be required";
                                        else if(is.not.email(values.email))
                                            errors.email = "Invalid Email";
                                        else if(values.email.length < 6)
                                            errors.email = "Email must > 6 characters";
                                        else if(values.email.length > 50)
                                            errors.email = "Email must < 50 characters";
                                        
                                        if(!values.password)
                                            errors.password = "Password must be required";
                                        else if(values.password.length < 6)
                                            errors.password = "Password must > 6 characters";
                                        else if(values.password.length > 50)
                                            errors.password = "Password must < 50 characters";
                                        return errors;
                                    }
                                }
                                render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                                    this.resetForm = form.reset;
                                    return <form onSubmit={handleSubmit}>
                                        <div className="uk-inline uk-width-1-1">
                                            {submitting && <Cover/>}
                                            {submitting && <Spinner/>}
                                            {this.state.isServerError && <Alert message={"Login Failed !!!"}
                                                onBeforeHide={() => this.setState({isServerError: false})}/>}
                                            <Field name="email">
                                                {({ input, meta }) => (
                                                <div className="uk-flex uk-flex-column uk-margin">
                                                    <div className="uk-inline uk-width-1-1">
                                                        <span className="uk-form-icon" uk-icon="icon: mail"/>
                                                        <InputText {...input} type="text" placeholder="Your Email"/>
                                                    </div>
                                                    {
                                                        meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error || meta.submitError}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <Field name="password">
                                                {({ input, meta }) => (
                                                <div className="uk-flex uk-flex-column uk-margin">
                                                    <InputPassword {...input} placeholder="Your Password"/>
                                                    {
                                                        meta.error && meta.touched && 
                                                        <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                            {meta.error}
                                                        </span>
                                                    }
                                                </div>
                                                )}
                                            </Field>
                                            <div className="uk-flex uk-margin-large-top">
                                                <Button className="uk-width-1-1 uk-button-large " type="submit" disabled={submitting || validating} color="primary">
                                                    Login
                                                </Button>
                                            </div>
                                        </div>
                                    </form>
                                }}>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Login;