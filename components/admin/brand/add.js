import React from "react";
import { Form, Field } from 'react-final-form';
import { Cover, Spinner, Button } from "../../../components/common";
import DeviceModel from "../../../models/device";

class BrandAdd extends React.Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:true
        }
        this.handlerSubmit = this.handlerSubmit.bind(this);
    }
    componentWillUnmount(){
        this.resetForm = null;
    }
    handlerSubmit(values){
        return new Promise((resolve, reject) => {
            this.setState({isLoading:true});
            DeviceModel.insertBrand({name: values.name})
            .then(() => {
                this.props.history.push('/brand/list');
            })
            .catch(() => {
                resolve();
            })
        })
    }
    render(){
        return(
            <div className="uk-padding-small">
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/brand/list')}>Brand</a></li>
                    <li><span>Add</span></li>
                </ul>
                <div>
                    <Form
                        onSubmit={this.handlerSubmit}
                        validate={values => {
                                const errors = {};
                                if(!values.name)
                                    errors.name = "Brand name must be required";
                                
                                return errors;
                            }
                        }
                        render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit}>
                                <div className="uk-inline uk-width-1-1">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    <Field name="name">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column">
                                            <div className="">
                                                <label className="uk-form-label uk-text-bold">Brand</label>
                                                <div className="uk-form-controls uk-margin-xsmall-top">
                                                    <input {...input} className="uk-input uk-width-1-2" type="text" placeholder="Brand..." />
                                                </div>
                                            </div>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>                           
                                    <div className="uk-flex uk-margin">
                                        <Button className="uk-button" type="submit" disabled={submitting || validating} color="primary">
                                            Save
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}>
                    </Form>
                </div>
            </div>
        )
    }
}
export default BrandAdd;