import React from "react";
import ReactTable from 'react-table';
import DeviceModel from "../../../models/device";
import Confirm from "../confirm";

class BrandList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            list : [],
            showConfirm: false
        }
        this.selectedId = 0;
    }
    componentDidMount(){
        this.listBrand();
    }
    componentWillUnmount(){
        this.selectedId = 0;
    }
    listBrand(){
        DeviceModel.getBrandList()
        .then(result =>{
            this.setState({list: result});
        })
    }
    onOpenConfirm(row){
        this.setState({ showConfirm: true }, () => {
            this.selectedId = row.value;
        });
    }
    onClose(){
        this.setState({ showConfirm: false }, () => {
            this.selectedId = 0;
        });
    }
    onDelete(){
        DeviceModel.deleteBrand({id: this.selectedId})
        .then(()=>{
            this.setState({showConfirm:false}, ()=>{
                this.listBrand();
                this.selectedId = 0;
            })
        })
    }
    render(){
        const data = this.state.list;

        const columns = [
            {
                Header: 'No.',
                id: 'row',
                maxWidth: 100,
                filterable: false,
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.index+1}</div>
                }
            },
            {
                Header: 'Name',
                accessor: 'name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header:'',
                accessor: 'id',
                Cell: (row) => {
                    return <div>
                        <ul className="uk-iconnav">
                            <li><a onClick={() => this.props.history.push('/brand/image/'+row.value)} uk-icon="icon: image"></a></li>
                            <li><a onClick={() => this.props.history.push('/brand/edit/'+row.value)} uk-icon="icon: file-edit" ></a></li>
                            <li><a onClick={this.onOpenConfirm.bind(this, row)} uk-icon="icon: trash"></a></li>
                        </ul>
                    </div>
                }
            }
        ];

        return (
            <div className="uk-padding-small">
                <Confirm show={this.state.showConfirm}
                    title="Are you sure Delete?"
                    onCancel={this.onClose.bind(this)}
                    onOK={this.onDelete.bind(this)}/>
                <div className="uk-flex uk-flex-between">
                    <ul className="uk-breadcrumb">
                        <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                        <li><span>Brand</span></li>
                    </ul>
                    <div>
                        <a onClick={() => this.props.history.push('/brand/add')} className="uk-link-reset"><span uk-icon="icon: plus-circle"></span> ADD NEW</a>
                    </div>
                </div>
                <ReactTable
                    data={data}
                    columns={columns}       
                    defaultPageSize={10}/>
            </div>
        )
    }
}
export default BrandList;