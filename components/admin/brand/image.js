import React from "react";
import DeviceModel from "../../../models/device";
import axios from "axios";
import { SERVER_API } from "../../../config";
import {ImageUpload} from "../../common";

class BrandImage extends React.Component{
    constructor(props){
        super(props);
        this.id = 0;    
        this.imageUpload = null;
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        this.handlerApiImages();
    }
    handlerApiImages(){
        this.imageUpload.beforeRefresh();
        DeviceModel.getBrandImage({id:this.id})
        .then(result => {
            this.imageUpload.refreshList(result);
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiDelete(){
        this.imageUpload.beforeRefresh();
        DeviceModel.deleteBrandImage({id: this.id})
        .then(result => {
            this.handlerApiImages();
        })
        .catch(error => {
            this.imageUpload.afterRefresh();
        })
    }
    handlerApiUploadImage(formData){
        return axios({
            method: 'post',
            url: SERVER_API+'brand/imageUpload',
            data: formData
        })
    }
    handlerServerUpload(files){
        const formData = new FormData();
        formData.append('photo', files[0]);
        formData.append('id', this.id);
        this.handlerApiUploadImage(formData)
        .then(() => {
            this.handlerApiImages();
        })
        .catch(error => {
            if(error.status === 400)
                UIkit.notification({message: 'This Image Cannot Upload', status: 'error', pos: 'top-left'});
        })
    }
    render(){
        return(
            <div className="uk-padding-small">
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/brand/list')}>Brand</a></li>
                    <li><span>Image</span></li>
                </ul>
                <div className="uk-container uk-margin-top">
                    {/* <h3 className="uk-margin-remove-top uk-margin-large">Images For {this.state.model}</h3> */}
                    <ImageUpload maxFiles={1}
                        folder="brands"
                        ref={instance => this.imageUpload = instance}
                        onChange={this.handlerServerUpload.bind(this)}
                        onDelete={this.handlerApiDelete.bind(this)}
                    />
                </div>
            </div>
        )
    }
}
export default BrandImage;



