import React from "react";
import {AutoAffix, Dropdown } from "react-overlays";
import axios from "axios";
import { withRouter } from "react-router";

class Toggle extends React.Component{
    constructor(props){ 
        super(props);
    }
    render(){
        return (
            <Dropdown.Toggle>
                {({ toggle, show, props }) => (
                    <div id={this.props.id} {...props} onClick={toggle}>
                        {this.props.children}
                    </div>
                )}
            </Dropdown.Toggle>
        )
    }
}

const Menu = ({ role }) => (
    <Dropdown.Menu flip>
        {({ show, onClose, props }) => {
            return (
                <div
                    {...props}
                    role={role}
                    style={{
                        display: show ? 'block': 'none',
                        position: 'absolute',
                        right: 0,
                        top: '70px',
                        zIndex: 1
                    }}>
                    <div className="uk-grid-small uk-flex-middle uk-background-default uk-box-shadow-medium uk-width-large" uk-grid="true">
                        <div className="uk-width-expand">
                            <form className="uk-search uk-search-navbar uk-width-1-1">
                                <a href="" className="uk-search-icon-flip" uk-search-icon="true"></a>
                                <input className="uk-search-input" type="search" placeholder="Search..." />
                            </form>
                        </div>
                    </div>
                </div>
            );
      }}
    </Dropdown.Menu>
  );

class DropdownSearch extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        const {show, onToggle, drop, role} = this.props;
        
        return (
            <Dropdown show={show}
                onToggle={onToggle}
                drop={drop}
                alignEnd>
                {
                    ({props}) => (
                        <div {...props}>
                            <Toggle><a uk-icon="icon: search"></a></Toggle>
                            <Menu role={role} />
                        </div>
                    )
                }
            </Dropdown>
        )
    }
}

class Header extends React.Component{
    constructor(...args) {
        super(...args);   
        this.state = { 
            show: false
        };
    }
    handlerLogout(){
        localStorage.removeItem('admin');
        localStorage.removeItem('token_admin');
        delete axios.defaults.headers.common["Authorization"];
        setTimeout(() => {
            console.log('sasa', this.props);
            this.props.history.push('/login');
            UIkit.notification({message: 'Logout Successfully !', status: 'primary', pos: 'top-left'});

        }, 500)
    }
    render(){
        return(
            <div>
                <AutoAffix viewportOffsetTop={0} container={this}>
                    <nav className="uk-box-shadow-medium uk-flex uk-flex-between uk-background-default uk-position-z-index" style={{height:'70px'}}>
                        <div className="uk-navbar-left"></div>
                        <div className="uk-navbar-right">
                            <div className="uk-margin-large-right">
                                <DropdownSearch 
                                    show={this.state.show}
                                    onToggle={show => this.setState({ show })}
                                    right />
                            </div>
                            <div className="uk-margin-large-right">
                                <a uk-icon="icon: bell"></a>
                            </div>
                            <div className="uk-margin-large-right">
                                <a>
                                    <img src="https://www.thegamerules.com/images/stories/virtuemart/product/funko-pop!-avengers-infinity-war---thanos-(289)-1.png" 
                                    className="uk-border-circle" uk-img="true" width="41px" height="41px" />
                                </a>
                            </div>
                            <div className="uk-margin-large-right">
                                <a uk-icon="icon: sign-out" onClick={this.handlerLogout.bind(this)}></a>
                            </div>
                        </div>
                    </nav>
                </AutoAffix>
                {this.props.children}
            </div>
        )
    }
}
export default withRouter(Header);