import React from "react";
import {Portal } from "react-overlays";
import {withRouter} from "react-router-dom";

import { Transition } from 'react-transition-group';

import {
    Accordion,
    AccordionItem,
    AccordionItemTitle,
    AccordionItemBody
} from 'react-accessible-accordion';

const duration = 150;
const defaultStyle = {
  transition: `opacity ${duration}ms ease-in-out`,
  opacity: 0,
}
const transitionStyles = {
  entering: { opacity: 1 },
  entered:  { opacity: 1 }
};

class SideMenu extends React.Component{
    constructor(...args) {
        super(...args);
        this.state = { 
            isExpanded: true
        };
    }

    goToMode(){
        this.props.onClickMode(!this.state.isExpanded);
        this.setState({isExpanded: !this.state.isExpanded});
    }
    
    render(){
        let content = (
            <Transition
                in={!this.state.isExpanded}
                timeout={150}
                unmountOnExit>
                {state => (
                    <div style={{
                            ...defaultStyle,
                            ...transitionStyles[state]
                        }}>
                        <div className="uk-offcanvas-bar uk-padding-remove" style={{transform: 'inherit', width:'60px',position:'fixed'}}>
                            <div className="uk-cursor uk-flex uk-flex-middle uk-background-dashboard uk-padding-remove-top uk-padding-remove-bottom" style={{height:'70px'}}>
                                <div>
                                    <span className="uk-padding-medium" onClick={this.goToMode.bind(this)}>
                                        <i className="fa fa-align-left" aria-hidden="true"></i>
                                    </span>    
                                </div>
                            </div>
                            <a className="uk-link-reset" onClick={() => this.props.history.push('/')}>
                                <div className="uk-padding-medium uk-component uk-margin-top">
                                    <div>
                                        <span><i className="fa fa-dashboard" aria-hidden="true"></i></span>    
                                    </div>                            
                                </div>
                            </a>
                            <a className="uk-link-reset" onClick={() => this.props.history.push('/slide/list')}>
                                <div className="uk-padding-medium uk-component">
                                    <div>
                                        <span><i className="fa fa-superpowers" aria-hidden="true"></i></span>    
                                    </div>                            
                                </div>
                            </a>
                            <div>
                                <div className="uk-text-small uk-padding-medium uk-text-muted ">...</div>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/category/list')}>
                                    <div className=" uk-padding-medium uk-component">
                                        <div>
                                            <span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/brand/list')}>
                                    <div className=" uk-padding-medium uk-component">
                                        <div>
                                            <span><i className="fa fa-indent" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/model/list')}>
                                    <div className=" uk-padding-medium uk-component">
                                        <div>
                                            <span><i className="fa fa-bandcamp" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/imei/list')}>
                                    <div className=" uk-padding-medium uk-component">
                                        <div>
                                            <span><i className="fa fa-barcode" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                                <a className="uk-link-reset">
                                    <div className=" uk-padding-medium uk-component">
                                        <div>
                                            <span><i className="fa fa-sliders" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        
                        </div>
                    </div>
                )}
            </Transition>
        )
        let expandedContent = (
            <Transition
                in={this.state.isExpanded}
                timeout={150}
                unmountOnExit>
                {state => (
                    <div style={{
                            ...defaultStyle,
                            ...transitionStyles[state]
                        }}>
                        <div>
                            <div className="uk-offcanvas-bar uk-padding-remove" style={{transform: 'inherit', width:'250px',position:'fixed'}}>
                                <div className="uk-cursor uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-top uk-padding-remove-bottom uk-padding-remove-right uk-background-dashboard" style={{height:'70px'}}>
                                    <div className="uk-text-white uk-text-large-more" onClick={() => this.props.history.push('/')}>SWAP-EZ</div>
                                    <div>
                                        <span className="uk-padding-small" onClick={this.goToMode.bind(this)}><i className="fa fa-align-right" aria-hidden="true"></i></span>    
                                    </div>
                                </div>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/')}>
                                    <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-component uk-margin-top">
                                        <div>
                                            <span><i className="fa fa-dashboard" aria-hidden="true"></i></span>
                                            <span className="uk-margin-xsmall-left">Dashboard</span>    
                                        </div>                            
                                    </div>
                                </a>
                                <a className="uk-link-reset" onClick={() => this.props.history.push('/slide/list')}>
                                    <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-component ">
                                        <div>
                                            <span><i className="fa fa-superpowers" aria-hidden="true"></i></span>
                                            <span className="uk-margin-xsmall-left">Slide</span>    
                                        </div>                            
                                    </div>
                                </a>
                                <div>
                                    <div className="uk-text-small uk-padding-medium uk-text-muted">Component</div>
                                    <Accordion accordion={false}>
                                        <AccordionItem>
                                            <AccordionItemTitle>
                                                <div className="uk-background-transparent">
                                                    <span><i className="fa fa-object-group" aria-hidden="true"></i></span>
                                                    <span className="uk-margin-xsmall-left">Device Imei</span>    
                                                </div>
                                            </AccordionItemTitle>
                                            <AccordionItemBody>
                                               <div>
                                                <a className="uk-link-reset" onClick={() => this.props.history.push('/category/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-pencil-square-o" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Category</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/brand/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-indent" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Brand</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/model/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-bandcamp" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Model</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/imei/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-barcode" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">IMEI</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                               </div>
                                            </AccordionItemBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionItemTitle>
                                                <div className="uk-background-transparent">
                                                    <span><i className="fa fa-info-circle" aria-hidden="true"></i></span>
                                                    <span className="uk-margin-xsmall-left">Device Parameter</span>    
                                                </div>
                                            </AccordionItemTitle>
                                            <AccordionItemBody>
                                                <div>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/color/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-th" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Color</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/capacity/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-recycle" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Capacity</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/ram/list')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-microchip" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Ram</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </AccordionItemBody>
                                        </AccordionItem>
                                        <AccordionItem>
                                            <AccordionItemTitle>
                                                <div className="uk-background-transparent">
                                                    <span><i className="fa fa-cogs" aria-hidden="true"></i></span>
                                                    <span className="uk-margin-xsmall-left">Settings</span>    
                                                </div>
                                            </AccordionItemTitle>
                                            <AccordionItemBody>
                                                <div>
                                                    <a className="uk-link-reset" onClick={() => this.props.history.push('/change-password')}>
                                                        <div className="uk-flex uk-flex-between uk-flex-middle uk-padding-medium uk-padding-remove-right  uk-component">
                                                            <div>
                                                                <span><i className="fa fa-lock" aria-hidden="true"></i></span>
                                                                <span className="uk-margin-xsmall-left">Change password</span>    
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </AccordionItemBody>
                                        </AccordionItem>
                                    </Accordion>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                )}
            </Transition>
        )
        return(
                <Portal container={() => document.getElementById('side-left-menu')}>
                    <div id="sidemenu">
                        {content}
                        {expandedContent}
                    </div>
                </Portal>
        )
    }
}
export default withRouter(SideMenu);