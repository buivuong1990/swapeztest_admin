import React from "react";
import { Form, Field } from 'react-final-form';
import { Cover, Spinner, Button } from "../../../components/common";
import DeviceModel from "../../../models/device";
import { Panel as ColorPickerPanel } from 'rc-color-picker';

class ColorEdit extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            name: '',
            code: '',
        }
        this.handlerSubmit = this.handlerSubmit.bind(this);
        
    }
    componentWillMount(){
        this.id = this.props.match.params.id;
    }
    componentDidMount(){
        DeviceModel.getColorDetail({id:this.id})
        .then(detail => {
            const {name, code} = detail;
            this.setState({name, code});
        })
    }
    handlerSubmit(values){
        this.setState({isLoading: true});
        DeviceModel.editColor({id:this.id, name:values.name, code: values.color})
        .then(()=>{
            this.setState({isLoading: false});
            UIkit.notification({message: 'Update Successfully !', status: 'primary', pos: 'top-left'});
            this.props.history.push('/color/list');
        })
    }
    render(){
        return(
            <div className="uk-padding-small">
                <ul className="uk-breadcrumb">
                    <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                    <li><a onClick={() => this.props.history.push('/color/list')}>Color</a></li>
                    <li><span>Edit</span></li>
                </ul>
                <div>
                    <Form
                        initialValues={{
                            name: this.state.name,
                            color:this.state.code
                        }}
                        onSubmit={this.handlerSubmit.bind(this)}
                        validate={values => {
                                const errors = {};
                                if(!values.name)
                                    errors.name = "Color name must be required";
                                
                                return errors;
                            }
                        }
                        render={({submitError, validating, handleSubmit, form, submitting, pristine, values}) => {
                            this.resetForm = form.reset;
                            return <form onSubmit={handleSubmit}>
                                <div className="uk-inline uk-width-1-1">
                                    {submitting && <Cover/>}
                                    {submitting && <Spinner/>}
                                    <Field name="name">
                                        {({ input, meta }) => (
                                        <div className="uk-flex uk-flex-column">
                                            <div className="">
                                                <label className="uk-form-label uk-text-bold">Color</label>
                                                <div className="uk-form-controls uk-margin-xsmall-top">
                                                    <input {...input} className="uk-input uk-width-1-2" type="text" placeholder="Color..." />
                                                </div>
                                            </div>
                                            {
                                                meta.error && meta.touched && 
                                                <span className="uk-text-meta uk-margin-xsmall-top uk-text-danger">
                                                    {meta.error}
                                                </span>
                                            }
                                        </div>
                                        )}
                                    </Field>
                                    <Field name="color">
                                        {({ input, meta }) => (
                                            <div className="uk-flex uk-flex-column">
                                            <label className="uk-form-label uk-text-bold uk-margin uk-margin-top">Color Picker</label>
                                                <ColorPickerPanel enableAlpha={false} 
                                                    color={input.value} 
                                                    onChange={(obj) => {
                                                        input.onChange(obj.color);
                                                    }} mode="RGB"/>
                                            </div>
                                        )}
                                    </Field>                        
                                    <div className="uk-flex uk-margin">
                                        <Button className="uk-button" type="submit" disabled={submitting || validating} color="primary">
                                            Save
                                        </Button>
                                    </div>
                                </div>
                            </form>
                        }}>
                    </Form>
                </div>
            </div>
        )
    }
}
export default ColorEdit;