import React from "react";
import ReactTable from 'react-table';
import DeviceModel from "../../../models/device";
import Confirm from "../confirm";

class CategoryList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            list : [],
            showConfirm: false,
            search: {
                name: ''
            }
        }
        this.selectedId = 0;
    }
    componentDidMount(){
        this.listCategory();
    }
    componentWillUnmount(){
        this.selectedId = 0;
    }
    listCategory(){
        DeviceModel.getCategoryList(this.state.search)
        .then(result =>{
            this.setState({list: result});
        })
    }
    onOpenConfirm(row){
        this.setState({ showConfirm: true }, () => {
            this.selectedId = row.value;
        });
    }
    onClose(){
        this.setState({ showConfirm: false }, () => {
            this.selectedId = 0;
        });
    }
    onDelete(){
        DeviceModel.deleteCategory({id: this.selectedId})
        .then(()=>{
            this.setState({showConfirm:false}, ()=>{
                this.listCategory();
                this.selectedId = 0;
            })
        })
    }
    handlerSearch(event){
        let search = Object.assign({}, this.state.search);
        search.name = event.target.value;
        this.setState({search: search});
    }
    handleKeyPress(event) {
        if (event.key === 'Enter') {
            this.onSearch();
        }
    }
    onSearch(){
        console.log('keypress', this.state.search);
    }
    render(){
        const data = this.state.list;

        const columns = [
            {
                Header: 'No.',
                id: 'row',
                maxWidth: 100,
                filterable: false,
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.index+1}</div>
                }
            },
            {
                Header: 'Name',
                accessor: 'name',
                Cell: (row) => {
                    return <div style={{textAlign:'center'}}>{row.value}</div>
                }
            },
            {
                Header:'',
                accessor: 'id',
                Cell: (row) => {
                    return <div>
                        <ul className="uk-iconnav">
                            <li><a onClick={() => this.props.history.push('/category/edit/'+row.value)} uk-icon="icon: file-edit" ></a></li>
                            <li><a onClick={this.onOpenConfirm.bind(this, row)} uk-icon="icon: trash"></a></li>
                        </ul>
                    </div>
                }
            }
        ];

        return (
            <div className="uk-padding-small">
                <Confirm show={this.state.showConfirm}
                    title="Are you sure Delete"
                    onCancel={this.onClose.bind(this)}
                    onOK={this.onDelete.bind(this)}/>
                <div className="uk-flex uk-flex-between">
                    <ul className="uk-breadcrumb">
                        <li><a onClick={() => this.props.history.push('/')}><i className="fa fa-home"></i> Dashboard</a></li>
                        <li><span>Category</span></li>
                    </ul>
                    <div>
                        <a onClick={() => this.props.history.push('/category/add')} className="uk-link-reset"><span uk-icon="icon: plus-circle"></span> ADD NEW</a>
                    </div>
                </div>
                <div className="uk-grid">
                    <div className="uk-width-1-2 uk-margin-bottom">
                        <div className="uk-inline uk-width-1-1">
                            <a className="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"
                                onClick={this.onSearch.bind(this)}/>
                            <input className="uk-input" type="search" placeholder="Search By Name"
                                onChange={this.handlerSearch.bind(this)}
                                onKeyPress={this.handleKeyPress.bind(this)}/>
                        </div>
                    </div>
                </div>
                <ReactTable
                    data={data}
                    columns={columns}       
                    defaultPageSize={10}/>
            </div>
        )
    }
}
export default CategoryList;