import React from "react";
import {Cover, Spinner} from "../index";

export class InputRange extends React.Component{
    constructor(props){
        super(props);
        this.instanceRef = null;
        this.range = null;
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.value)
            this.range.setValue(0, nextProps.value);
    }
    componentWillUnmount(){
        this.range = null;
    }
    componentDidMount(){
        this.range = new JSR([this.instanceRef], {
            sliders: 1,
            values: [100],
            step: 1,
            limit: {
                min: 50
            },
            labels: {
                formatter: (value) => {
                    return value.toString() + ' %';
                }
            },
            grid: false
        }).addEventListener('update', (input, value) => {
            if(is.function(this.props.onUpdate)){
                this.props.onUpdate(value);
            }
        });
    }
    render(){
        const min = this.props.min ? this.props.min : 0;
        const max = this.props.max ? this.props.max : 100;
        return (
            <div className="uk-inline uk-width-1-1" style={{width: '97%'}}>
                <input ref={instance => this.instanceRef = instance} id={this.props.id} type="range" min={min} 
                    max={max}/>
            </div>
        )
    }
}

export class InputText extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
        this.state = {
            value: ''
        }
    }
    handlerInput(value){
        this.setState({value: value});
    }
    handlerClassInput(){
        const {className} = this.props;
        let classList = 'uk-input';
        is.existy(className) ? classList += ' '+className: classList;
        return classList;
    }
    render(){
        return (
            <input className={this.handlerClassInput()}
                onChange={(event) => this.handlerInput(event.target.value)}
                ref={instance => this.inputRef = instance}
                value={this.state.value}
                autoComplete="off"
                disabled={this.props.disabled}
                {...this.props}/>
        )
    }
}

export class InputNumber extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
    }
    handlerInput(event){
        let value = event.target.value;
        let valueNum = Number(event.target.value);
        if(is.not.number(valueNum)){
            if (value != null && value.length > 0) {
                value = value.substring(0, value.length - 1);
            }
        }
        event.target.value = value;
    }
    render(){
        return (
            <div className="uk-inline uk-width-1-1">
                {
                    this.props.icon ?
                    <span className="uk-form-icon" uk-icon={"icon: "+this.props.icon}/>
                    : null
                }
                <input className="uk-input" type="number"
                    onInput={this.handlerInput}
                    {...this.props}/>
            </div>
        )
    }
}

export class InputPassword extends React.Component{
    constructor(props){
        super(props);
        this.handlerInput = this.handlerInput.bind(this);
        this.handlerToggle = this.handlerToggle.bind(this);
        this.state = {
            isShow: false,
            value: '',
        }
        this.inputRef = null;
    }
    componentWillUnmount(){
        this.inputRef = null;
    }
    handlerInput(value){
        this.setState({value: value});
    }
    handlerToggle(){
        this.setState({isShow: !this.state.isShow}, () => {
            $(this.inputRef).focus();
        });
    }
    handlerClassInput(){
        const {className} = this.props;
        let classList = 'uk-input';
        is.existy(className) ? classList += ' '+className: classList;
        return classList;
    }
    renderEye(){
        if(is.not.ie() && is.not.edge())
            return this.state.isShow ? <i className="fa fa-eye"/> : <i className="fa fa-eye-slash"/>
        else return null;
    }
    render(){
        return (
            <div className="uk-inline uk-width-1-1">
                <span className="uk-form-icon" uk-icon={"icon: lock"}/>
                <input className={this.handlerClassInput()} type={this.state.isShow ? 'text': 'password'}
                    onInput={event => this.handlerInput(event.target.value)}
                    ref={instance => this.inputRef = instance}
                    autoComplete="new-password"
                    {...this.props}/>
                <a className="uk-form-icon uk-link-reset uk-form-icon-flip" onClick={this.handlerToggle}>
                    {
                        this.renderEye()
                    }
                </a>
            </div>
        )
    }
}

export class Checkbox extends React.Component{
    constructor(props){
        super(props);   
    }
    setCheck(){
        $(this.instance).trigger('click');
    }
    setSelected(){
        $(this.instance).prop('checked', true);
    }
    setUnSelected(){
        $(this.instance).prop('checked', false);
    }
    render(){
        const mode = this.props.mode ? this.props.mode : 'default';
        if(mode === 'default')
            return (
                <div className="pretty p-svg" style={{fontSize: this.props.fontSize ? this.props.fontSize : 'inherit'}}>
                    <input type="checkbox" {...this.props} ref={instance => this.instance = instance}/>
                    <div className="state">
                        <span className="svg uk-background-secondary uk-text-white" uk-icon="icon: check"></span>
                        <label>
                            <span className="uk-margin-xsmall-left">{this.props.label}</span>
                        </label>
                    </div>
                </div>
            )
        else if(mode === 'heart')
            return (
                <div className="pretty p-icon p-toggle p-plain" style={{fontSize: this.props.fontSize ? this.props.fontSize : 'inherit'}}>
                    <input type="checkbox" {...this.props}/>
                    <div className="state p-off">
                        <i className="icon fa fa-heart"/>
                        <label>{this.props.label}</label>
                    </div>
                    <div className="state p-on p-info-o">
                        <i className="icon fa fa-heart"/>
                        <label>{this.props.label}</label>
                    </div>
                </div>
            )
    }
}

export class Radio extends React.Component{
    constructor(props){
        super(props);   
    }
    render(){
        const mode = this.props.mode ? this.props.mode : 'default';
        if(mode === 'default')
            return (
                <div className="pretty p-icon p-toggle p-plain" style={{fontSize: this.props.fontSize ? this.props.fontSize : 'inherit'}}>
                    <input type="radio" {...this.props}/>
                    <div className="state p-off">
                        <i className="icon fa fa-check"/>
                        <label>{this.props.label}</label>
                    </div>
                    <div className="state p-on p-info-o">
                        <i className="icon fa fa-check"/>
                        <label>{this.props.label}</label>
                    </div>
                </div>
            )
        else
            return (
                <div className="pretty p-svg p-round p-jelly" style={{fontSize: this.props.fontSize ? this.props.fontSize : 'inherit'}}>
                    <input type="radio" {...this.props}/>
                    <div className="state p-success">
                        <div className="uk-flex uk-flex-middle">
                            <span className="svg" uk-icon="icon: check"></span>
                            <label><div className="uk-text-bold uk-margin-xsmall-left">{this.props.label}</div></label>
                        </div>
                    </div>
                </div>
            )
    }
}

import Select from 'react-select';

export class Select2 extends React.Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div className="uk-inline uk-width-1-1">
                <Select
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    onChange={this.props.onChange}
                    options={this.props.options}
                    getOptionLabel={(option) => {
                        return option[this.props.optionLabel];
                    }}
                    getOptionValue={(option) => {
                        return option[this.props.optionValue];
                    }}
                />
            </div>
        )
    }
}