import "babel-polyfill";

import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import axios from "axios";

import io from 'socket.io-client';

import createHistory from "history/createBrowserHistory";
import { Provider } from "react-redux";
import configureStore from "./configureStore";

import {SERVER_SOCKET} from "../config";
import Functions from "../functions";

import SideMenu from "../components/admin/sidemenu";
import Header from "../components/admin/header";

import CategoryAdd from "../components/admin/category/add";
import CategoryList from "../components/admin/category/list";
import CategoryEdit from "../components/admin/category/edit";

import BrandList from "../components/admin/brand/list";
import BrandAdd from "../components/admin/brand/add";
import BrandEdit from "../components/admin/brand/edit";
import BrandImage from "../components/admin/brand/image";

import ColorList from "../components/admin/color/list";
import ColorAdd from "../components/admin/color/add";
import ColorEdit from "../components/admin/color/edit";

import CapacityList from "../components/admin/capacity/list";
import CapacityAdd from "../components/admin/capacity/add";
import CapacityEdit from "../components/admin/capacity/edit";

import RamList from "../components/admin/ram/list";
import RamAdd from "../components/admin/ram/add";
import RamEdit from "../components/admin/ram/edit";

import ModelList from "../components/admin/model/list";
import ModelAdd from "../components/admin/model/add";
import ModelEdit from "../components/admin/model/edit";
import ModelImage from "../components/admin/model/image";

import ImeiList from "../components/admin/imei/list";
import ImeiAdd from "../components/admin/imei/add";
import ImeiEdit from "../components/admin/imei/edit";

import SlideList from "../components/admin/slide/list";
import SlideAdd from "../components/admin/slide/add";
import SlideEdit from "../components/admin/slide/edit";
import SlideImage from "../components/admin/slide/image";

import Login from "../components/admin/login";
import ChangePassword from "../components/admin/changePassword";

import withStorage from "../hoc/storage";

const initialState = {};
const history = createHistory();
const store = configureStore(initialState, history);

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isMenu: true
        }
        this.observer = null;
    }
    componentWillMount(){
        accounting.settings = {
            currency: {
                symbol : "$",   // default currency symbol is '$'
                format: "%s%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
                decimal : ".",  // decimal point separator
                thousand: ",",  // thousands separator
                precision : 0   // decimal places
            },
            number: {
                precision : 0,  // default precision on numbers is 0
                thousand: ",",
                decimal : "."
            }
        }
        axios.interceptors.response.use(function (response) {
            const data = response.data;
            if(is.undefined(data)){
                UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
                return Promise.reject({status: 500});
            }
            else if(data.status !== 200)
                return Promise.reject(data);
            else
                return data.data;
        }, function(error) {
            UIkit.notification({message: 'Server are on construction !', status: 'danger', pos: 'top-left'});
            return Promise.reject({status: 500});
        });

        const connectionOptions = {
            'force new connection': true,
            'reconnectionAttemps': 'Infinity',
            transports: ['websocket']
        }

        this.socket = io.connect(SERVER_SOCKET, connectionOptions);
        Functions.socketReceivedProposal(this.socket)
        .subscribe((observer) => {
            this.observer = observer;
            UIkit.notification({message: 'Your Proposal List Has Changed !', status: 'primary', pos: 'top-left'});
        })
    }
    componentWillUnmount(){
        if(this.observer)
            this.observer.complete();
    }
    setMenu(){
        this.setState({isMenu: false}, () => {
            this.setState({isMenu: true});
        });
    }
    _onClickMode(mode){
        if(mode)
            $(this.menu).css('paddingLeft', '250px');
        else
            $(this.menu).css('paddingLeft', '60px');
    }
    render(){
        return (
            <Provider store={store}>
                <Router>
                    <div>
                        <SideMenu onClickMode={this._onClickMode.bind(this)}/>
                        <Route exact path="/login" component={Login}/>
                        <div ref={obj => this.menu = obj} style={{paddingLeft:'250px'}}>
                            <Header>
                                <React.Fragment>
                                    <Route exact path="/category/list" component={CategoryList}/>
                                    <Route exact path="/category/add" component={CategoryAdd}/>
                                    <Route exact path="/category/edit/:id" component={CategoryEdit}/>
                                    <Route exact path="/brand/list" component={BrandList}/>
                                    <Route exact path="/brand/add" component={BrandAdd}/>
                                    <Route exact path="/brand/edit/:id" component={BrandEdit}/>
                                    <Route exact path="/brand/image/:id" component={BrandImage}/>
                                    <Route exact path="/color/list" component={ColorList}/>
                                    <Route exact path="/color/add" component={ColorAdd}/>
                                    <Route exact path="/color/edit/:id" component={ColorEdit}/>
                                    <Route exact path="/capacity/list" component={CapacityList}/>
                                    <Route exact path="/capacity/add" component={CapacityAdd}/>
                                    <Route exact path="/capacity/edit/:id" component={CapacityEdit}/>
                                    <Route exact path="/ram/list" component={RamList}/>
                                    <Route exact path="/ram/add" component={RamAdd}/>
                                    <Route exact path="/ram/edit/:id" component={RamEdit}/>
                                    <Route exact path="/model/list" component={ModelList}/>
                                    <Route exact path="/model/add" component={ModelAdd}/>
                                    <Route exact path="/model/edit/:id" component={ModelEdit}/>
                                    <Route exact path="/model/image/:id" component={ModelImage}/>
                                    <Route exact path="/imei/list" component={ImeiList}/>
                                    <Route exact path="/imei/add" component={ImeiAdd}/>
                                    <Route exact path="/imei/edit/:id" component={ImeiEdit}/>
                                    <Route exact path="/slide/list" component={SlideList}/>
                                    <Route exact path="/slide/add" component={SlideAdd}/>
                                    <Route exact path="/slide/edit/:id" component={SlideEdit}/>
                                    <Route exact path="/slide/image/:id" component={SlideImage}/>
                                    <Route exact path="/change-password" component={ChangePassword}/>
                                </React.Fragment>
                            </Header>
                        </div> 
                    </div>
                </Router>
            </Provider>
        )
    }
}

export default App;