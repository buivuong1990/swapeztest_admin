let api = 'http://localhost';
let socket = 'http://localhost';

if(WEBPACK_MODE === 'production'){
    api = 'http://demo.swap-ez.com';
    socket = 'http://demo.swap-ez.com';
}else if(WEBPACK_MODE === 'thuy'){
    api = 'http://192.168.1.54';
    socket = 'http://192.168.1.54';
}

export const SERVER_API = api+':3003/api/';
export const SERVER_DOMAIN = api+':3003/';
export const SERVER_SOCKET = `${socket}:8091`;

export const SERVER_ANONYMOUS = 'http://ipinfo.io';
export const SERVER_IMG = 'http://localhost:3003/';
export const SERVER_UPLOAD = 'http://localhost:7000/';



export const COUNTRY_LIST = [
    {id: 1, name: 'Canada'},
    {id: 2, name: 'England'},
    {id: 3, name: 'USA'}
];

export const QUESTION_LIST = [
    {id: 1, name: 'What is your parent name ?'},
    {id: 2, name: 'What is your favorite drink ?'},
    {id: 3, name: "What is your pet name ?"}
];

export const RECAPTCHA_SITE_KEY = '6Le1m3cUAAAAALV7RMhLQ35tgU_UlRm8XrIKReis';

export const MICRO_DOMAIN = api+':';
export const MICRO_AUTH = 3001;
export const MICRO_CART = 3002;
export const MICRO_DEVICE = 3003;
export const MICRO_ORDER = 3004;
export const MICRO_EVENT = 3005;
